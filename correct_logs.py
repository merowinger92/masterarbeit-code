# logs .csv files have a wrong header ID; Fitness\n instead of ID; Individuals; Fitness\n
import os
import glob
import time

if __name__ == "__main__":
    start = time.time()
    path_regex = "**/run-*"
    files = glob.glob(path_regex, recursive=True)
    lwn_files = len(files)
    for file in files:
        with open(file, 'U') as f:
            content = f.read()
            content = content.replace("ID; Fitness", "ID; Individuals; Fitness")
        with open(file, 'w') as f:
            f.write(content)
        print(file)
    end = time.time()
    print("Took " + str(end-start) + "seconds to replace everything")
