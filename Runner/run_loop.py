import time


def run(agents, env, routine, inject_routine = None, max_frames=0):
    """
    similar to run_loop (# based on https://github.com/deepmind/pysc2/blob/master/pysc2/env/run_loop.py) but a single run

    :param agents: list fo agents to interact with env
    :param env: StarCraft II Environment
    :param routine: the GP generated Routine to use
    :param max_frames: maximum amunt of frames
    :return: reward obtained in one run
    """
    reward = 0
    total_frames = 0
    start_time = time.time()

    observation_spec = env.observation_spec()
    action_spec = env.action_spec()
    observations = []

    for agent, obs_spec, act_spec in zip(agents, observation_spec, action_spec):
        agent.setup(obs_spec=obs_spec, action_spec=act_spec)

    timesteps = env.reset()
    for a in agents:
        a.reset()

    try:
        while True:
            total_frames += 1
            actions = [agent.step(timestep, routine, inject_routine) for agent, timestep in zip(agents, timesteps)]
            # assumption: it is always only one agent
            observations = observations + [timesteps[0]]
            if max_frames and total_frames >= max_frames:
                return
            if timesteps[0].last():
                reward = timesteps[0].observation["score_cumulative"][0]
                break
            timesteps = env.step(actions)
    except KeyboardInterrupt:
        pass
    finally:
        elapsed_time = time.time() - start_time
        print("Took %.3f seconds for %s steps: %.3f fps" % (elapsed_time, total_frames, total_frames / elapsed_time))

    run_result = {"observations": observations,
                  "reward": timesteps[0].observation["score_cumulative"][0]}
    return run_result


def run_optimum_loop(agents, env, max_frames=0):
    """
    similar to run_loop (# based on https://github.com/deepmind/pysc2/blob/master/pysc2/env/run_loop.py) but a single run

    :param agents: list fo agents to interact with env
    :param env: StarCraft II Environment
    :param routine: the GP generated Routine to use
    :param max_frames: maximum amunt of frames
    :return: reward obtained in one run
    """
    reward = 0
    total_frames = 0
    start_time = time.time()

    observation_spec = env.observation_spec()
    action_spec = env.action_spec()

    for agent, obs_spec, act_spec in zip(agents, observation_spec, action_spec):
        agent.setup(obs_spec=obs_spec, action_spec=act_spec)

    timesteps = env.reset()
    for a in agents:
        a.reset()

    try:
        while True:
            total_frames += 1
            actions = [agent.optimum_algorithm(timestep) for agent, timestep in zip(agents, timesteps)]

            if max_frames and total_frames >= max_frames:
                return
            if timesteps[0].last():
                reward = timesteps[0].observation["score_cumulative"][0]
                break
            timesteps = env.step(actions)
    except KeyboardInterrupt:
        pass
    finally:
        elapsed_time = time.time() - start_time
        print("Took %.3f seconds for %s steps: %.3f fps" % (elapsed_time, total_frames, total_frames / elapsed_time))

    return reward


def run_loop(agents, env, max_frames=0, max_episodes=1):
    # based on https://github.com/deepmind/pysc2/blob/master/pysc2/env/run_loop.py
    total_frames = 0
    total_episodes = 0
    start_time = time.time()

    observation_spec = env.observation_spec()
    action_spec = env.action_spec()
    for agent, obs_spec, act_spec in zip(agents, observation_spec, action_spec):
        agent.setup(obs_spec, act_spec)

    try:
        while not max_episodes or total_episodes < max_episodes:
            total_episodes += 1
            timesteps = env.reset()
            for a in agents:
                a.reset()
            while True:
                total_frames += 1
                actions = [agent.step(timestep)
                        for agent, timestep in zip(agents, timesteps)]
                if max_frames and total_frames >= max_frames:
                    return
                if timesteps[0].last():
                    break
                timesteps = env.step(actions)
    except KeyboardInterrupt:
        pass
    finally:
        elapsed_time = time.time() - start_time
        print("Took %.3f seconds for %s steps: %.3f fps" % (elapsed_time, total_frames, total_frames / elapsed_time))

