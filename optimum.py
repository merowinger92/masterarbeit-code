from absl import app
from absl import flags

from pysc2.env import sc2_env
from pysc2.env import available_actions_printer

from pysc2.lib import point_flag
from pysc2.lib import protocol
from pysc2.lib import remote_controller

from GeneticAgent import DefeatRoaches
from GeneticAgent import MoveToBeacon
from Runner import run_loop

FLAGS = flags.FLAGS
flags.DEFINE_bool("render", False, "Whether to render with pygame.")
point_flag.DEFINE_point("feature_screen_size", "84",
                        "Resolution for screen feature layers.")
point_flag.DEFINE_point("feature_minimap_size", "64",
                        "Resolution for minimap feature layers.")
point_flag.DEFINE_point("rgb_screen_size", None,
                        "Resolution for rendered screen.")
point_flag.DEFINE_point("rgb_minimap_size", None,
                        "Resolution for rendered minimap.")
flags.DEFINE_enum("action_space", None, sc2_env.ActionSpace._member_names_,  # pylint: disable=protected-access
                  "Which action space to use. Needed if you take both feature "
                  "and rgb observations.")
flags.DEFINE_bool("use_feature_units", False,
                  "Whether to include feature units.")
flags.DEFINE_bool("disable_fog", False, "Whether to disable Fog of War.")

flags.DEFINE_integer("max_agent_steps", 0, "Total agent steps.")
flags.DEFINE_integer("game_steps_per_episode", None, "Game steps per episode.")
flags.DEFINE_integer("max_episodes", 0, "Total episodes.")
flags.DEFINE_integer("step_mul", 22, "Game steps per agent step.")

flags.DEFINE_string("agent", "pysc2.agents.scripted_agent.MoveToBeacon",
                    "Which agent to run, as a python path to an Agent class.")
flags.DEFINE_enum("agent_race", "random", sc2_env.Race._member_names_,  # pylint: disable=protected-access
                  "Agent 1's race.")

flags.DEFINE_string("agent2", "Bot", "Second agent, either Bot or agent class.")
flags.DEFINE_enum("agent2_race", "random", sc2_env.Race._member_names_,  # pylint: disable=protected-access
                  "Agent 2's race.")
flags.DEFINE_enum("difficulty", "very_easy", sc2_env.Difficulty._member_names_,  # pylint: disable=protected-access
                  "If agent2 is a built-in Bot, it's strength.")

flags.DEFINE_bool("profile", False, "Whether to turn on code profiling.")
flags.DEFINE_bool("trace", False, "Whether to trace the code execution.")
flags.DEFINE_integer("parallel", 1, "How many instances to run in parallel.")

flags.DEFINE_bool("save_replay", True, "Whether to save a replay at the end.")

flags.DEFINE_string("map", "DefeatRoaches", "Name of a map to use.")


def main(unused_args):
    ant = DefeatRoaches.DefeatRoaches()

    players = []
    agents = []
    agents.append(ant)

    reward = 0
    players.append(sc2_env.Agent(sc2_env.Race["random"]))  # just add player with random race...
    not_finished = True

    while not_finished:
        try:
            # TODO: parameterize everything...
            with sc2_env.SC2Env(
                    map_name=FLAGS.map,
                    players=players,
                    agent_interface_format=sc2_env.parse_agent_interface_format(
                        feature_screen=FLAGS.feature_screen_size,
                        feature_minimap=FLAGS.feature_minimap_size,
                        rgb_screen=FLAGS.rgb_screen_size,
                        rgb_minimap=FLAGS.rgb_minimap_size,
                        action_space=FLAGS.action_space,
                        use_feature_units=FLAGS.use_feature_units),
                    step_mul=FLAGS.step_mul,
                    game_steps_per_episode=FLAGS.game_steps_per_episode,
                    disable_fog=FLAGS.disable_fog,
                    visualize=FLAGS.render) as env:
                env = available_actions_printer.AvailableActionsPrinter(env)
                reward += run_loop.run_optimum_loop(agents, env)

            not_finished = False

        except remote_controller.ConnectError:
            print("ConnectError Exception occured!")
            pass
        except protocol.ConnectionError:
            print("ConnectionError Exception occured!")
            pass
        except protocol.ProtocolError:
            print("ProtocolError: Exception occured")
            pass
        except KeyboardInterrupt:
            print("KeyboardInterrupt")
            reward = float("-inf")
            not_finished = False  # no second try with this individual
            pass
        except Exception as exception:  # all other exceptions are caught here
            print("Exception: ", repr(exception))
            reward = float("-inf")
            not_finished = False  # no second try with this individual
            pass

    print("current reward: ", reward)


if __name__ == "__main__":
    app.run(main)
