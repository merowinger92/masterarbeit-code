import random
import importlib
import numpy
import os

from absl import app
from absl import flags

from pysc2.env import sc2_env
from pysc2.env import available_actions_printer

from pysc2.lib import point_flag
from pysc2.lib import protocol
from pysc2.lib import remote_controller
import pickle

# from deap import algorithms
from deapAlgorithms import eaSimpleModified as algorithms
from deap import base
from deap import creator
from deap import tools
from deap import gp

from common import genetitcTypes, genticFunctions
import GeneticAgent.MoveToBeacon
from Runner import run_loop
from log import logger
from pysc2.lib import actions

from pysc2.lib.actions import Function
import numpy as np
import multiprocessing
from common.exceptions import NotAvailableActionException

from common import config
from common import utils

FUNCTIONS = actions.FUNCTIONS

FLAGS = flags.FLAGS
flags.DEFINE_bool("render", False, "Whether to render with pygame.")
point_flag.DEFINE_point("feature_screen_size", "84",
                        "Resolution for screen feature layers.")
point_flag.DEFINE_point("feature_minimap_size", "64",
                        "Resolution for minimap feature layers.")
point_flag.DEFINE_point("rgb_screen_size", None,
                        "Resolution for rendered screen.")
point_flag.DEFINE_point("rgb_minimap_size", None,
                        "Resolution for rendered minimap.")
flags.DEFINE_enum("action_space", None, sc2_env.ActionSpace._member_names_,  # pylint: disable=protected-access
                  "Which action space to use. Needed if you take both feature "
                  "and rgb observations.")
flags.DEFINE_bool("use_feature_units", True,
                  "Whether to include feature units.")
flags.DEFINE_bool("disable_fog", False, "Whether to disable Fog of War.")

flags.DEFINE_integer("max_agent_steps", 0, "Total agent steps.")
flags.DEFINE_integer("game_steps_per_episode", None, "Game steps per episode.")
flags.DEFINE_integer("max_episodes", 0, "Total episodes.")
flags.DEFINE_integer("step_mul", 8, "Game steps per agent step.")

flags.DEFINE_string("agent", "GeneticAgent.MoveToBeacon.MoveToBeacon",
                    "Which agent to run, as a python path to an Agent class.")
flags.DEFINE_enum("agent_race", "random", sc2_env.Race._member_names_,  # pylint: disable=protected-access
                  "Agent 1's race.")

flags.DEFINE_string("agent2", "Bot", "Second agent, either Bot or agent class.")
flags.DEFINE_enum("agent2_race", "random", sc2_env.Race._member_names_,  # pylint: disable=protected-access
                  "Agent 2's race.")
flags.DEFINE_enum("difficulty", "very_easy", sc2_env.Difficulty._member_names_,  # pylint: disable=protected-access
                  "If agent2 is a built-in Bot, it's strength.")

flags.DEFINE_bool("profile", False, "Whether to turn on code profiling.")
flags.DEFINE_bool("trace", False, "Whether to trace the code execution.")
flags.DEFINE_integer("parallel", 1, "How many instances to run in parallel.")

flags.DEFINE_bool("save_replay", True, "Whether to save a replay at the end.")
flags.DEFINE_string("map", "MoveToBeacon", "Name of a map to use.")

GLOBAL = utils.Globals()

GLOBAL.all_seen_funcs = set()

ant = GeneticAgent.MoveToBeacon.MoveToBeacon()

pset = gp.PrimitiveSetTyped("MAIN", [np.ndarray, np.ndarray], Function, "IN")
pset.renameArguments(ARG0="player_relative")
pset.renameArguments(ARG1="selected")

primitive_list = ant.return_primitives_and_terminals()
for primitve in primitive_list:
    if len(primitve) > 3:  # primitive
        if len(primitve[1]) > 0:
            pset.addPrimitive(primitve[0], primitve[1], primitve[2], primitve[3])
        else:
            pset.addTerminal(primitve[0], primitve[2], primitve[3])
    else:  # terminal
        pset.addTerminal(primitve[0], primitve[1], primitve[2])

creator.create("FitnessMax", base.Fitness, weights=(1.0,))  # positive weight =>max/ negative weight=>min
creator.create("Individual", gp.PrimitiveTree, fitness=creator.FitnessMax)


ind_to_evaluate = "if_then_else(exists_value_in_matrix(generic_helper_primitive(generic_helper_primitive(" \
                  "generic_helper_primitive(generic_helper_primitive(generic_helper_primitive(" \
                  "generic_helper_primitive(generic_helper_primitive(generic_helper_primitive(IN1))))))))," \
                  " generic_helper_primitive(generic_helper_primitive(generic_helper_primitive(" \
                  "generic_helper_primitive(generic_helper_primitive(generic_helper_primitive(1))))))), " \
                  "attack_screen(exists_value_in_matrix(generic_helper_primitive(generic_helper_primitive(" \
                  "generic_helper_primitive(generic_helper_primitive(generic_helper_primitive(" \
                  "generic_helper_primitive(generic_helper_primitive(helper_ndarray))))))), " \
                  "generic_helper_primitive(generic_helper_primitive(0))), generic_helper_primitive(" \
                  "search_single_center_in_points(search_value_in_matrix(generic_helper_primitive(" \
                  "generic_helper_primitive(generic_helper_primitive(generic_helper_primitive(IN0)))), " \
                  "generic_helper_primitive(generic_helper_primitive(generic_helper_primitive(3))))))), " \
                  "select_marine(generic_helper_primitive(generic_helper_primitive(exists_value_in_matrix(" \
                  "generic_helper_primitive(IN0), generic_helper_primitive(generic_helper_primitive(1)))))))"

# ind = gp.compile(ind_to_evaluate, pset)


def evalArtificialAnt(individual, eval_stage=0, inject_str=None, inject_pset=None):
    # print(individual)
    players = []
    agents = []
    routine = gp.compile(individual, pset)
    agents.append(ant)

    fitness = float("-100")
    players.append(sc2_env.Agent(sc2_env.Race["random"]))  # just add player with random race... TODO:

    inject_algo = None
    if inject_str is not None:
        inject_algo = gp.compile(inject_str, inject_pset)

    eval_stage_counter = "eval_stage-" + str(eval_stage)

    not_finished = True
    while not_finished:
        try:
            # TODO: parameterize everything...
            with sc2_env.SC2Env(
                        map_name=FLAGS.map,
                        players=players,
                        agent_interface_format=sc2_env.parse_agent_interface_format(
                            feature_screen=FLAGS.feature_screen_size,
                            feature_minimap=FLAGS.feature_minimap_size,
                            rgb_screen=FLAGS.rgb_screen_size,
                            rgb_minimap=FLAGS.rgb_minimap_size,
                            action_space=FLAGS.action_space,
                            use_feature_units=FLAGS.use_feature_units),
                        step_mul=FLAGS.step_mul,
                        game_steps_per_episode=FLAGS.game_steps_per_episode,
                        disable_fog=FLAGS.disable_fog,
                        visualize=FLAGS.render) as env:
                    env = available_actions_printer.AvailableActionsPrinter(env)
                    # global INJECT
                    results = run_loop.run(agents, env, routine, inject_algo)
                    # global EVAL_STAGE
                    evaluation_args = {
                        "results_of_run": results,
                        "eval_stage": eval_stage
                    }
                    for func in env._seen:
                        # global all_seen_funcs
                        if func not in GLOBAL.all_seen_funcs:
                            GLOBAL.all_seen_funcs.add(func)
                    fitness = ant.evaluate(evaluation_args)

            not_finished = False
            # logger.log_exception_and_success("success", individual, eval_stage_counter)
        except remote_controller.ConnectError:
            print("ConnectError Exception occured!")
            pass
        except protocol.ConnectionError:
            print("ConnectionError Exception occured!")
            pass
        except protocol.ProtocolError:
            print("ProtocolError: Exception occured")
            pass
        except NotAvailableActionException:
            # Exception is raised when the algorithm returnes an unavailable action
            print("NotAvailableActionException: ", individual)
            fitness = float("-100")
            # found
            not_finished = False  # no second try with this individual
            pass
        except NotImplementedError:
            fitness = float("-50000")
            # found
            not_finished = False  # no second try with this individual
            pass
        except KeyboardInterrupt:
            print("KeyboardInterrupt")
            fitness = float("-100")
            not_finished = False  # no second try with this individual
            pass
        except Exception as exception:  # all other exceptions are caught here
            print("Exception: ", repr(exception), " individual: ", individual)
            # logger.log_exception_and_success(repr(exception), individual, eval_stage_counter)
            fitness = float("-100")
            not_finished = False  # no second try with this individual
            pass

    return fitness,


def main(unused_args):
    evalArtificialAnt(ind_to_evaluate, 1)


if __name__ == "__main__":
    app.run(main)
