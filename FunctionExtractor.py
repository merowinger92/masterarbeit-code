
pattern = '    def {func_name_lower}(self, {args}):\n'
pattern += '        """\n'
pattern += '        Function ID: {func_id}\n'
pattern += '        :param arg_bool: {description1}\n'
pattern += '        :param point: {description2}\n'
pattern += '        :return: the selection FunctionCall to hand to the game\n'
pattern += '        """\n'
pattern += '        return FUNCTIONS.{func_name_orig}({args})\n\n'

pattern1 = '    def {func_name_lower}(self, {args}):\n'
pattern1 += '        """\n'
pattern1 += '        Function ID: {func_id}\n'
pattern1 += '        :param arg_bool: {description1}\n'
pattern1 += '        :return: the selection FunctionCall to hand to the game\n'
pattern1 += '        """\n'
pattern1 += '        return FUNCTIONS.{func_name_orig}({args})\n\n'

pattern2 = '    def {func_name_lower}(self):\n'
pattern2 += '        """\n'
pattern2 += '        Function ID: {func_id}\n'
pattern2 += '        :return: the selection FunctionCall to hand to the game\n'
pattern2 += '        """\n'
pattern2 += '        return FUNCTIONS.{func_name_orig}({args})\n\n'


def extract_function_and_args(line_of_files):
        idx_func_start = line_of_files.find('"') + 1
        idx_func_last = line_of_files.rfind('"')
        function_name = line_of_files[idx_func_start:idx_func_last]
        args_name = line_of_files.split(',')[2]
        idx_func_id_start = line_of_files.find('(') +1
        idx_func_id_end = line_of_files.find(',')
        func_id_ = line_of_files[idx_func_id_start:idx_func_id_end]
        return func_id_, function_name, args_name


def return_description(arg_of_pysc2):
    description1 = ""
    description2 = ""
    if arg_of_pysc2 == " cmd_screen":
        description2 = "Point to attack on the screen"
    elif arg_of_pysc2 == " cmd_minimap":
        description2 = "Point to attack on the minimap"
    elif arg_of_pysc2 == " cmd_quick":
        description1 = "arg_bool: True/false value if action should be queued after other actions"
    elif arg_of_pysc2 == " autocast":
        pass
    else:
        raise Exception
    return description1, description2


def return_args(arg_of_pysc2):
    args_to_hand_over = ""
    if arg_of_pysc2 == " cmd_screen":
        args_to_hand_over = "arg_bool, point"
    elif arg_of_pysc2 == " cmd_minimap":
        args_to_hand_over = "arg_bool, point"
    elif arg_of_pysc2 == " cmd_quick":
        args_to_hand_over = "arg_bool"
    elif arg_of_pysc2 == " autocast":
        pass
    else:
        raise Exception
    return args_to_hand_over


def build_func_pattern(_func_id, function_name, args_name_from_pysc2):
    pattern_to_use = pattern
    if args_name_from_pysc2 == " cmd_quick":
        pattern_to_use = pattern1
    if args_name_from_pysc2 == " autocast":
        pattern_to_use = pattern2

    description1, description2 = return_description(args_name_from_pysc2)
    final_args = return_args(args_name_from_pysc2)
    pattern_to_use = pattern_to_use.replace("{func_name_lower}", function_name.lower())
    pattern_to_use = pattern_to_use.replace("{args}", final_args)
    pattern_to_use = pattern_to_use.replace("{func_id}", _func_id)
    pattern_to_use = pattern_to_use.replace("{description1}", description1)
    pattern_to_use = pattern_to_use.replace("{description2}", description2)
    pattern_to_use = pattern_to_use.replace("{func_name_orig}", function_name)
    return pattern_to_use


if __name__ == "__main__":
    lines = []
    with open('Functions.txt', 'r') as f:
        for line in f:
            func_id, act, arg = extract_function_and_args(line)
            new_line = build_func_pattern(func_id, act, arg)
            lines.append(new_line)

    with open('FunctionGenerated.txt', 'w') as outfile:
        for line in lines:
            outfile.write(line)

    print("ready")
