replacements = {"helper_ndarray":                   "genticFunctions.helper_ndarray",
                "if_then_else":                     "genticFunctions.if_then_else",
                "move_player_to_point":             "self.move_player_to_point",
                "exists_value_in_matrix":           "self.exists_value_in_matrix",
                "search_value_in_matrix":           "self.search_value_in_matrix",
                "search_single_center_in_points":   "self.search_single_center_in_points",
                "select_marine":                    "self.select_marine",
                "do_nothing":                       "self.do_nothing",
                "IN0":                              "player_relative",
                "IN1":                              "selected_player",
                "generic_helper_primitive":         "genticFunctions.generic_helper_primitive"}

lines = []
with open('output.log') as infile:
    for line in infile:
        for src, target in replacements.items():
            line = line.replace(src, target)
        lines.append(line)
with open('output.log', 'w') as outfile:
    for line in lines:
        outfile.write(line)