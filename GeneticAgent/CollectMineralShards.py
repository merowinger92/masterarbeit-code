from GeneticAgent import GeneticBaseAgent


class CollectMineralShards(GeneticBaseAgent.GeneticBaseAgent):

    def __init__(self):
        super(CollectMineralShards, self).__init__()
        self.user_func_id = {}

    def evaluate(self, args_to_evaluate):
        return 1 + 2

    def step(self, obs, routine, inject=None):
        action = super(CollectMineralShards, self).step(obs, routine)
        return action

    def return_primitives_and_terminals(self):
        pass

    def optimum_algorithm(self, obs):
        pass
