from pysc2.agents import base_agent
from pysc2.lib import actions
from pysc2.lib.actions import Function
import numpy as np
from deap import gp

from common import genetitcTypes

FUNCTIONS = actions.FUNCTIONS


class GeneticBaseAgent(base_agent.BaseAgent):

    def __init__(self):
        super(GeneticBaseAgent, self).__init__()
        self.first_print = True

    def return_primitives_and_terminals(self):
        # this method should only return terminals and primitives used for specific agents
        raise NotImplementedError("This method has to be implemented in derived classes")

    def evaluate(self, args_to_evaluate):
        """
        This method should be implemented in a derived class
        :param args_to_evaluate: can be a dictionary or what ever
        :return: the fitness Value for the routine
        """
        # this method be implemented by every single agent
        raise NotImplementedError("This method has to be implemented in derived classes")

    def setup(self, obs_spec, action_spec):
        super(GeneticBaseAgent, self).setup(obs_spec, action_spec)

    def optimum_algorithm(self, obs):
        raise NotImplementedError("This method has to be implemented in derived classes")

    def step(self, obs):
        return FUNCTIONS.no_op()

    def step(self, obs, routine, inject=None):
        """

        :param obs: observations from starcraft 2
        :param routine: routine which was generated and should be executed now
        :param inject: anything to inject
        :return:
        """
        super(GeneticBaseAgent, self).step(obs)
        # do if nothing is implemented nothing
        return FUNCTIONS.no_op()

    # ########matrix operations START#########
    def exists_value_in_matrix(self, matrix, value_to_search):
        """

            :param matrix: numpy.ndarray of an observation (in MoveToBeacon Case)
            :param value_to_search: the value to search in param matrix... used to be a value of pysc2.lib.features.PlayerRelative
            :return: True/False if value exists in that matrix
        """
        result = (matrix == value_to_search)
        return np.any(result)

    def search_value_in_matrix(self, matrix, value_to_search):
        """

            :param matrix: an observation matrix
            :param value_to_search: value to search for in matrix (a value in pysc2.lib.features.PlayerRelative)
            :return: list of [x,y]
        """
        res = (matrix == value_to_search)
        y_where_value_is_located, x_where_value_is_located = np.nonzero(res)
        result_to_return = list(zip(x_where_value_is_located, y_where_value_is_located))
        return result_to_return

    # ########matrix operations END#########

    def do_nothing(self):
        """
        Function ID: 0
        :return: returns StarCraft II Function which does nothing
        """
        return FUNCTIONS.no_op()

    def move_camera(self, point_to_move_to):
        """
        Function ID: 1
        :param point_to_move_to point where the camera should be moved to
        Moves the camera to a point on the minimap

        :return: returns the StarCraft II Function to move the camera to a minimap position
        """

        return FUNCTIONS.move_camera(point_to_move_to)

    def select_point(self, select_pt_opts, pt_on_screen):
        """
        Function ID: 2

        Selects a point pt_on_screen with the election options select_pt_opts. Possible Values are:
            "select",
            "toggle",
            "select_all_type",
            "add_all_type"
        :return: the StarCraft II Function to select a point with options
        """
        return FUNCTIONS.select_point(select_pt_opts, pt_on_screen)

    def select_rect(self, select_or_add, pt1, pt2):
        """
        Function ID: 3

        :param select_or_add: param equal to pysc2.lib.actions.SELECT_ADD_OPTIONS, True = add to existing (selected) army,
                         False = single select unit
        :param pt1: Point 1 for starting the recangle
        :param pt2: Point 2 for finishing the rectangle
        :return: the StarCraft II Function to select a rectangle
        """

        return FUNCTIONS.select_rect(select_or_add, pt1, pt2)

    def select_control_group(self, ctrl_grp_opts, ctrl_grp_id):
        """
        Function ID: 4

        :param ctrl_grp_opts: one of the following values:
            "recall",
            "set",
            "append",
            "set_and_steal"
            "append_and_steal"
        :param ctrl_grp_id: maximum 10 control groups... range 0-9 or 1-10...needs to be evaluated TODO:
        :return: the StarCarftII Function to form control groups
        """

        return FUNCTIONS.select_control_group(ctrl_grp_opts, ctrl_grp_id)

    def select_unit(self, select_unit_action, select_unit_id):
        """

        Function ID: 5

        :param select_unit_action: following values are possible:
                                        "select",
                                        "deselect",
                                        "select_all_type",
                                        "deselect_all_type",

        :param select_unit_id: Unit id... range from 0 to 499 or 1 to 500...needs to be evaluated TODO:
        :return: StarCraftII Function to select units...
        """
        return FUNCTIONS.select_unit(select_unit_action, select_unit_id)

    def select_idle_worker(self, select_worker_opts):
        """

        Function ID: 6

        :param select_worker_opts: following values are possible:
                                    "select",
                                    "add",
                                    "select_all",
                                    "add_all",
                                    equals to geneticType.SelectWorkerOpts
        :return: StarCraft II Function to select worker

        """

        return FUNCTIONS.select_worker(select_worker_opts)

    def select_marine(self, arg_bool):
        """
        Function ID: 7
        :param arg_bool: param equal to pysc2.lib.actions.SELECT_ADD_OPTIONS, True = add to existing (selected) army,
                         False = single select unit
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.select_army(arg_bool)

    def select_warp_gates(self, arg_bool):
        """
        Function ID: 8
        :param arg_bool: param equal to pysc2.lib.actions.SELECT_ADD_OPTIONS, True = add to existing (selected) army,
                         False = single select unit
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.select_warp_gates(arg_bool)

    def select_larva(self):
        """
        Function ID: 9
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.select_larva()

    def unload(self, unload_unit_id):
        """
        Function ID: 10
        :param unload_unit_id: Unit id... range from 0 to 499 or 1 to 500...needs to be evaluated TODO:
        :return:  the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.unload(unload_unit_id)

    def build_queue(self, build_queue_id):
        """
        Function ID: 11
        :param build_queue_id:maximum 10 control groups... range 0-9 or 1-10...needs to be evaluated TODO:
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.build_queue(build_queue_id)

    def attack_screen(self, arg_bool, point):
        """
        Function ID: 12
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Attack_screen(arg_bool, point)

    def attack_minimap(self, arg_bool, point):
        """
        Function ID: 13
        :param arg_bool:
        :param point: Point to attack on the minimap
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Attack_minimap(arg_bool, point)

    def attack_attack_screen(self, arg_bool, point):
        """
        Function ID: 14
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Attack_Attack_screen(arg_bool, point)

    def attack_attack_minimap(self, arg_bool, point):
        """
        Function ID: 15
        :param arg_bool:
        :param point: Point to attack on the minimap
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Attack_Attack_minimap(arg_bool, point)

    def attack_attackbuilding_screen(self, arg_bool, point):
        """
        Function ID: 16
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Attack_AttackBuilding_screen(arg_bool, point)

    def attack_attackbuilding_minimap(self, arg_bool, point):
        """
        Function ID: 17
        :param arg_bool:
        :param point: Point to attack on the minimap
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Attack_AttackBuilding_minimap(arg_bool, point)

    def attack_redirect_screen(self, arg_bool, point):
        """
        Function ID: 18
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Attack_Redirect_screen(arg_bool, point)

    def scan_move_screen(self, arg_bool, point):
        """
        Function ID: 19
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Scan_Move_screen(arg_bool, point)

    def scan_move_minimap(self, arg_bool, point):
        """
        Function ID: 20
        :param arg_bool:
        :param point: Point to attack on the minimap
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Scan_Move_minimap(arg_bool, point)

    def behavior_buildingattackoff_quick(self, arg_bool):
        """
        Function ID: 21
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Behavior_BuildingAttackOff_quick(arg_bool)

    def behavior_buildingattackon_quick(self, arg_bool):
        """
        Function ID: 22
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Behavior_BuildingAttackOn_quick(arg_bool)

    def behavior_cloakoff_quick(self, arg_bool):
        """
        Function ID: 23
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Behavior_CloakOff_quick(arg_bool)

    def behavior_cloakoff_banshee_quick(self, arg_bool):
        """
        Function ID: 24
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Behavior_CloakOff_Banshee_quick(arg_bool)

    def behavior_cloakoff_ghost_quick(self, arg_bool):
        """
        Function ID: 25
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Behavior_CloakOff_Ghost_quick(arg_bool)

    def behavior_cloakon_quick(self, arg_bool):
        """
        Function ID: 26
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Behavior_CloakOn_quick(arg_bool)

    def behavior_cloakon_banshee_quick(self, arg_bool):
        """
        Function ID: 27
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Behavior_CloakOn_Banshee_quick(arg_bool)

    def behavior_cloakon_ghost_quick(self, arg_bool):
        """
        Function ID: 28
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Behavior_CloakOn_Ghost_quick(arg_bool)

    def behavior_generatecreepoff_quick(self, arg_bool):
        """
        Function ID: 29
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Behavior_GenerateCreepOff_quick(arg_bool)

    def behavior_generatecreepon_quick(self, arg_bool):
        """
        Function ID: 30
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Behavior_GenerateCreepOn_quick(arg_bool)

    def behavior_holdfireoff_quick(self, arg_bool):
        """
        Function ID: 31
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Behavior_HoldFireOff_quick(arg_bool)

    def behavior_holdfireoff_ghost_quick(self, arg_bool):
        """
        Function ID: 32
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Behavior_HoldFireOff_Ghost_quick(arg_bool)

    def behavior_holdfireoff_lurker_quick(self, arg_bool):
        """
        Function ID: 33
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Behavior_HoldFireOff_Lurker_quick(arg_bool)

    def behavior_holdfireon_quick(self, arg_bool):
        """
        Function ID: 34
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Behavior_HoldFireOn_quick(arg_bool)

    def behavior_holdfireon_ghost_quick(self, arg_bool):
        """
        Function ID: 35
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Behavior_HoldFireOn_Ghost_quick(arg_bool)

    def behavior_holdfireon_lurker_quick(self, arg_bool):
        """
        Function ID: 36
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Behavior_HoldFireOn_Lurker_quick(arg_bool)

    def behavior_pulsarbeamoff_quick(self, arg_bool):
        """
        Function ID: 37
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Behavior_PulsarBeamOff_quick(arg_bool)

    def behavior_pulsarbeamon_quick(self, arg_bool):
        """
        Function ID: 38
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Behavior_PulsarBeamOn_quick(arg_bool)

    def build_armory_screen(self, arg_bool, point):
        """
        Function ID: 39
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_Armory_screen(arg_bool, point)

    def build_assimilator_screen(self, arg_bool, point):
        """
        Function ID: 40
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_Assimilator_screen(arg_bool, point)

    def build_banelingnest_screen(self, arg_bool, point):
        """
        Function ID: 41
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_BanelingNest_screen(arg_bool, point)

    def build_barracks_screen(self, arg_bool, point):
        """
        Function ID: 42
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_Barracks_screen(arg_bool, point)

    def build_bunker_screen(self, arg_bool, point):
        """
        Function ID: 43
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_Bunker_screen(arg_bool, point)

    def build_commandcenter_screen(self, arg_bool, point):
        """
        Function ID: 44
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_CommandCenter_screen(arg_bool, point)

    def build_creeptumor_screen(self, arg_bool, point):
        """
        Function ID: 45
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_CreepTumor_screen(arg_bool, point)

    def build_creeptumor_queen_screen(self, arg_bool, point):
        """
        Function ID: 46
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_CreepTumor_Queen_screen(arg_bool, point)

    def build_creeptumor_tumor_screen(self, arg_bool, point):
        """
        Function ID: 47
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_CreepTumor_Tumor_screen(arg_bool, point)

    def build_cyberneticscore_screen(self, arg_bool, point):
        """
        Function ID: 48
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_CyberneticsCore_screen(arg_bool, point)

    def build_darkshrine_screen(self, arg_bool, point):
        """
        Function ID: 49
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_DarkShrine_screen(arg_bool, point)

    def build_engineeringbay_screen(self, arg_bool, point):
        """
        Function ID: 50
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_EngineeringBay_screen(arg_bool, point)

    def build_evolutionchamber_screen(self, arg_bool, point):
        """
        Function ID: 51
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_EvolutionChamber_screen(arg_bool, point)

    def build_extractor_screen(self, arg_bool, point):
        """
        Function ID: 52
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_Extractor_screen(arg_bool, point)

    def build_factory_screen(self, arg_bool, point):
        """
        Function ID: 53
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_Factory_screen(arg_bool, point)

    def build_fleetbeacon_screen(self, arg_bool, point):
        """
        Function ID: 54
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_FleetBeacon_screen(arg_bool, point)

    def build_forge_screen(self, arg_bool, point):
        """
        Function ID: 55
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_Forge_screen(arg_bool, point)

    def build_fusioncore_screen(self, arg_bool, point):
        """
        Function ID: 56
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_FusionCore_screen(arg_bool, point)

    def build_gateway_screen(self, arg_bool, point):
        """
        Function ID: 57
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_Gateway_screen(arg_bool, point)

    def build_ghostacademy_screen(self, arg_bool, point):
        """
        Function ID: 58
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_GhostAcademy_screen(arg_bool, point)

    def build_hatchery_screen(self, arg_bool, point):
        """
        Function ID: 59
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_Hatchery_screen(arg_bool, point)

    def build_hydraliskden_screen(self, arg_bool, point):
        """
        Function ID: 60
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_HydraliskDen_screen(arg_bool, point)

    def build_infestationpit_screen(self, arg_bool, point):
        """
        Function ID: 61
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_InfestationPit_screen(arg_bool, point)

    def build_interceptors_quick(self, arg_bool):
        """
        Function ID: 62
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_Interceptors_quick(arg_bool)

    def build_interceptors_autocast(self):
        """
        Function ID: 63
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_Interceptors_autocast()

    def build_lurkerden_screen(self, arg_bool, point):
        """
        Function ID: 524
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_LurkerDen_screen(arg_bool, point)

    def build_missileturret_screen(self, arg_bool, point):
        """
        Function ID: 64
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_MissileTurret_screen(arg_bool, point)

    def build_nexus_screen(self, arg_bool, point):
        """
        Function ID: 65
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_Nexus_screen(arg_bool, point)

    def build_nuke_quick(self, arg_bool):
        """
        Function ID: 66
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_Nuke_quick(arg_bool)

    def build_nydusnetwork_screen(self, arg_bool, point):
        """
        Function ID: 67
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_NydusNetwork_screen(arg_bool, point)

    def build_nydusworm_screen(self, arg_bool, point):
        """
        Function ID: 68
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_NydusWorm_screen(arg_bool, point)

    def build_photoncannon_screen(self, arg_bool, point):
        """
        Function ID: 69
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_PhotonCannon_screen(arg_bool, point)

    def build_pylon_screen(self, arg_bool, point):
        """
        Function ID: 70
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_Pylon_screen(arg_bool, point)

    def build_reactor_quick(self, arg_bool):
        """
        Function ID: 71
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_Reactor_quick(arg_bool)

    def build_reactor_screen(self, arg_bool, point):
        """
        Function ID: 72
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_Reactor_screen(arg_bool, point)

    def build_reactor_barracks_quick(self, arg_bool):
        """
        Function ID: 73
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_Reactor_Barracks_quick(arg_bool)

    def build_reactor_barracks_screen(self, arg_bool, point):
        """
        Function ID: 74
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_Reactor_Barracks_screen(arg_bool, point)

    def build_reactor_factory_quick(self, arg_bool):
        """
        Function ID: 75
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_Reactor_Factory_quick(arg_bool)

    def build_reactor_factory_screen(self, arg_bool, point):
        """
        Function ID: 76
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_Reactor_Factory_screen(arg_bool, point)

    def build_reactor_starport_quick(self, arg_bool):
        """
        Function ID: 77
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_Reactor_Starport_quick(arg_bool)

    def build_reactor_starport_screen(self, arg_bool, point):
        """
        Function ID: 78
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_Reactor_Starport_screen(arg_bool, point)

    def build_refinery_screen(self, arg_bool, point):
        """
        Function ID: 79
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_Refinery_screen(arg_bool, point)

    def build_roachwarren_screen(self, arg_bool, point):
        """
        Function ID: 80
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_RoachWarren_screen(arg_bool, point)

    def build_roboticsbay_screen(self, arg_bool, point):
        """
        Function ID: 81
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_RoboticsBay_screen(arg_bool, point)

    def build_roboticsfacility_screen(self, arg_bool, point):
        """
        Function ID: 82
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_RoboticsFacility_screen(arg_bool, point)

    def build_sensortower_screen(self, arg_bool, point):
        """
        Function ID: 83
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_SensorTower_screen(arg_bool, point)

    def build_shieldbattery_screen(self, arg_bool, point):
        """
        Function ID: 525
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_ShieldBattery_screen(arg_bool, point)

    def build_spawningpool_screen(self, arg_bool, point):
        """
        Function ID: 84
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_SpawningPool_screen(arg_bool, point)

    def build_spinecrawler_screen(self, arg_bool, point):
        """
        Function ID: 85
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_SpineCrawler_screen(arg_bool, point)

    def build_spire_screen(self, arg_bool, point):
        """
        Function ID: 86
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_Spire_screen(arg_bool, point)

    def build_sporecrawler_screen(self, arg_bool, point):
        """
        Function ID: 87
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_SporeCrawler_screen(arg_bool, point)

    def build_stargate_screen(self, arg_bool, point):
        """
        Function ID: 88
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_Stargate_screen(arg_bool, point)

    def build_starport_screen(self, arg_bool, point):
        """
        Function ID: 89
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_Starport_screen(arg_bool, point)

    def build_stasistrap_screen(self, arg_bool, point):
        """
        Function ID: 90
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_StasisTrap_screen(arg_bool, point)

    def build_supplydepot_screen(self, arg_bool, point):
        """
        Function ID: 91
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_SupplyDepot_screen(arg_bool, point)

    def build_techlab_quick(self, arg_bool):
        """
        Function ID: 92
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_TechLab_quick(arg_bool)

    def build_techlab_screen(self, arg_bool, point):
        """
        Function ID: 93
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_TechLab_screen(arg_bool, point)

    def build_techlab_barracks_quick(self, arg_bool):
        """
        Function ID: 94
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_TechLab_Barracks_quick(arg_bool)

    def build_techlab_barracks_screen(self, arg_bool, point):
        """
        Function ID: 95
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_TechLab_Barracks_screen(arg_bool, point)

    def build_techlab_factory_quick(self, arg_bool):
        """
        Function ID: 96
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_TechLab_Factory_quick(arg_bool)

    def build_techlab_factory_screen(self, arg_bool, point):
        """
        Function ID: 97
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_TechLab_Factory_screen(arg_bool, point)

    def build_techlab_starport_quick(self, arg_bool):
        """
        Function ID: 98
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_TechLab_Starport_quick(arg_bool)

    def build_techlab_starport_screen(self, arg_bool, point):
        """
        Function ID: 99
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_TechLab_Starport_screen(arg_bool, point)

    def build_templararchive_screen(self, arg_bool, point):
        """
        Function ID: 100
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_TemplarArchive_screen(arg_bool, point)

    def build_twilightcouncil_screen(self, arg_bool, point):
        """
        Function ID: 101
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_TwilightCouncil_screen(arg_bool, point)

    def build_ultraliskcavern_screen(self, arg_bool, point):
        """
        Function ID: 102
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Build_UltraliskCavern_screen(arg_bool, point)

    def burrowdown_quick(self, arg_bool):
        """
        Function ID: 103
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowDown_quick(arg_bool)

    def burrowdown_baneling_quick(self, arg_bool):
        """
        Function ID: 104
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowDown_Baneling_quick(arg_bool)

    def burrowdown_drone_quick(self, arg_bool):
        """
        Function ID: 105
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowDown_Drone_quick(arg_bool)

    def burrowdown_hydralisk_quick(self, arg_bool):
        """
        Function ID: 106
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowDown_Hydralisk_quick(arg_bool)

    def burrowdown_infestor_quick(self, arg_bool):
        """
        Function ID: 107
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowDown_Infestor_quick(arg_bool)

    def burrowdown_infestorterran_quick(self, arg_bool):
        """
        Function ID: 108
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowDown_InfestorTerran_quick(arg_bool)

    def burrowdown_lurker_quick(self, arg_bool):
        """
        Function ID: 109
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowDown_Lurker_quick(arg_bool)

    def burrowdown_queen_quick(self, arg_bool):
        """
        Function ID: 110
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowDown_Queen_quick(arg_bool)

    def burrowdown_ravager_quick(self, arg_bool):
        """
        Function ID: 111
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowDown_Ravager_quick(arg_bool)

    def burrowdown_roach_quick(self, arg_bool):
        """
        Function ID: 112
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowDown_Roach_quick(arg_bool)

    def burrowdown_swarmhost_quick(self, arg_bool):
        """
        Function ID: 113
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowDown_SwarmHost_quick(arg_bool)

    def burrowdown_ultralisk_quick(self, arg_bool):
        """
        Function ID: 114
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowDown_Ultralisk_quick(arg_bool)

    def burrowdown_widowmine_quick(self, arg_bool):
        """
        Function ID: 115
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowDown_WidowMine_quick(arg_bool)

    def burrowdown_zergling_quick(self, arg_bool):
        """
        Function ID: 116
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowDown_Zergling_quick(arg_bool)

    def burrowup_quick(self, arg_bool):
        """
        Function ID: 117
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowUp_quick(arg_bool)

    def burrowup_autocast(self):
        """
        Function ID: 118
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowUp_autocast()

    def burrowup_baneling_quick(self, arg_bool):
        """
        Function ID: 119
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowUp_Baneling_quick(arg_bool)

    def burrowup_baneling_autocast(self):
        """
        Function ID: 120
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowUp_Baneling_autocast()

    def burrowup_drone_quick(self, arg_bool):
        """
        Function ID: 121
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowUp_Drone_quick(arg_bool)

    def burrowup_hydralisk_quick(self, arg_bool):
        """
        Function ID: 122
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowUp_Hydralisk_quick(arg_bool)

    def burrowup_hydralisk_autocast(self):
        """
        Function ID: 123
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowUp_Hydralisk_autocast()

    def burrowup_infestor_quick(self, arg_bool):
        """
        Function ID: 124
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowUp_Infestor_quick(arg_bool)

    def burrowup_infestorterran_quick(self, arg_bool):
        """
        Function ID: 125
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowUp_InfestorTerran_quick(arg_bool)

    def burrowup_infestorterran_autocast(self):
        """
        Function ID: 126
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowUp_InfestorTerran_autocast()

    def burrowup_lurker_quick(self, arg_bool):
        """
        Function ID: 127
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowUp_Lurker_quick(arg_bool)

    def burrowup_queen_quick(self, arg_bool):
        """
        Function ID: 128
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowUp_Queen_quick(arg_bool)

    def burrowup_queen_autocast(self):
        """
        Function ID: 129
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowUp_Queen_autocast()

    def burrowup_ravager_quick(self, arg_bool):
        """
        Function ID: 130
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowUp_Ravager_quick(arg_bool)

    def burrowup_ravager_autocast(self):
        """
        Function ID: 131
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowUp_Ravager_autocast()

    def burrowup_roach_quick(self, arg_bool):
        """
        Function ID: 132
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowUp_Roach_quick(arg_bool)

    def burrowup_roach_autocast(self):
        """
        Function ID: 133
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowUp_Roach_autocast()

    def burrowup_swarmhost_quick(self, arg_bool):
        """
        Function ID: 134
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowUp_SwarmHost_quick(arg_bool)

    def burrowup_ultralisk_quick(self, arg_bool):
        """
        Function ID: 135
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowUp_Ultralisk_quick(arg_bool)

    def burrowup_ultralisk_autocast(self):
        """
        Function ID: 136
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowUp_Ultralisk_autocast()

    def burrowup_widowmine_quick(self, arg_bool):
        """
        Function ID: 137
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowUp_WidowMine_quick(arg_bool)

    def burrowup_zergling_quick(self, arg_bool):
        """
        Function ID: 138
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowUp_Zergling_quick(arg_bool)

    def burrowup_zergling_autocast(self):
        """
        Function ID: 139
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.BurrowUp_Zergling_autocast()

    def cancel_quick(self, arg_bool):
        """
        Function ID: 140
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_quick(arg_bool)

    def cancel_adeptphaseshift_quick(self, arg_bool):
        """
        Function ID: 141
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_AdeptPhaseShift_quick(arg_bool)

    def cancel_adeptshadephaseshift_quick(self, arg_bool):
        """
        Function ID: 142
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_AdeptShadePhaseShift_quick(arg_bool)

    def cancel_barracksaddon_quick(self, arg_bool):
        """
        Function ID: 143
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_BarracksAddOn_quick(arg_bool)

    def cancel_buildinprogress_quick(self, arg_bool):
        """
        Function ID: 144
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_BuildInProgress_quick(arg_bool)

    def cancel_creeptumor_quick(self, arg_bool):
        """
        Function ID: 145
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_CreepTumor_quick(arg_bool)

    def cancel_factoryaddon_quick(self, arg_bool):
        """
        Function ID: 146
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_FactoryAddOn_quick(arg_bool)

    def cancel_gravitonbeam_quick(self, arg_bool):
        """
        Function ID: 147
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_GravitonBeam_quick(arg_bool)

    def cancel_lockon_quick(self, arg_bool):
        """
        Function ID: 148
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_LockOn_quick(arg_bool)

    def cancel_morphbroodlord_quick(self, arg_bool):
        """
        Function ID: 149
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_MorphBroodlord_quick(arg_bool)

    def cancel_morphgreaterspire_quick(self, arg_bool):
        """
        Function ID: 150
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_MorphGreaterSpire_quick(arg_bool)

    def cancel_morphhive_quick(self, arg_bool):
        """
        Function ID: 151
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_MorphHive_quick(arg_bool)

    def cancel_morphlair_quick(self, arg_bool):
        """
        Function ID: 152
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_MorphLair_quick(arg_bool)

    def cancel_morphlurker_quick(self, arg_bool):
        """
        Function ID: 153
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_MorphLurker_quick(arg_bool)

    def cancel_morphlurkerden_quick(self, arg_bool):
        """
        Function ID: 154
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_MorphLurkerDen_quick(arg_bool)

    def cancel_morphmothership_quick(self, arg_bool):
        """
        Function ID: 155
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_MorphMothership_quick(arg_bool)

    def cancel_morphorbital_quick(self, arg_bool):
        """
        Function ID: 156
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_MorphOrbital_quick(arg_bool)

    def cancel_morphoverlordtransport_quick(self, arg_bool):
        """
        Function ID: 157
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_MorphOverlordTransport_quick(arg_bool)

    def cancel_morphoverseer_quick(self, arg_bool):
        """
        Function ID: 158
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_MorphOverseer_quick(arg_bool)

    def cancel_morphplanetaryfortress_quick(self, arg_bool):
        """
        Function ID: 159
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_MorphPlanetaryFortress_quick(arg_bool)

    def cancel_morphravager_quick(self, arg_bool):
        """
        Function ID: 160
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_MorphRavager_quick(arg_bool)

    def cancel_morphthorexplosivemode_quick(self, arg_bool):
        """
        Function ID: 161
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_MorphThorExplosiveMode_quick(arg_bool)

    def cancel_neuralparasite_quick(self, arg_bool):
        """
        Function ID: 162
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_NeuralParasite_quick(arg_bool)

    def cancel_nuke_quick(self, arg_bool):
        """
        Function ID: 163
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_Nuke_quick(arg_bool)

    def cancel_spinecrawlerroot_quick(self, arg_bool):
        """
        Function ID: 164
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_SpineCrawlerRoot_quick(arg_bool)

    def cancel_sporecrawlerroot_quick(self, arg_bool):
        """
        Function ID: 165
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_SporeCrawlerRoot_quick(arg_bool)

    def cancel_starportaddon_quick(self, arg_bool):
        """
        Function ID: 166
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_StarportAddOn_quick(arg_bool)

    def cancel_stasistrap_quick(self, arg_bool):
        """
        Function ID: 167
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_StasisTrap_quick(arg_bool)

    def cancel_last_quick(self, arg_bool):
        """
        Function ID: 168
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_Last_quick(arg_bool)

    def cancel_hangarqueue5_quick(self, arg_bool):
        """
        Function ID: 169
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_HangarQueue5_quick(arg_bool)

    def cancel_queue1_quick(self, arg_bool):
        """
        Function ID: 170
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_Queue1_quick(arg_bool)

    def cancel_queue5_quick(self, arg_bool):
        """
        Function ID: 171
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_Queue5_quick(arg_bool)

    def cancel_queueaddon_quick(self, arg_bool):
        """
        Function ID: 172
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_QueueAddOn_quick(arg_bool)

    def cancel_queuecanceltoselection_quick(self, arg_bool):
        """
        Function ID: 173
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_QueueCancelToSelection_quick(arg_bool)

    def cancel_queuepasive_quick(self, arg_bool):
        """
        Function ID: 174
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_QueuePasive_quick(arg_bool)

    def cancel_queuepassivecanceltoselection_quick(self, arg_bool):
        """
        Function ID: 175
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Cancel_QueuePassiveCancelToSelection_quick(arg_bool)

    def effect_abduct_screen(self, arg_bool, point):
        """
        Function ID: 176
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_Abduct_screen(arg_bool, point)

    def effect_adeptphaseshift_screen(self, arg_bool, point):
        """
        Function ID: 177
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_AdeptPhaseShift_screen(arg_bool, point)

    def effect_antiarmormissile_screen(self, arg_bool, point):
        """
        Function ID: 526
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_AntiArmorMissile_screen(arg_bool, point)

    def effect_autoturret_screen(self, arg_bool, point):
        """
        Function ID: 178
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_AutoTurret_screen(arg_bool, point)

    def effect_blindingcloud_screen(self, arg_bool, point):
        """
        Function ID: 179
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_BlindingCloud_screen(arg_bool, point)

    def effect_blink_screen(self, arg_bool, point):
        """
        Function ID: 180
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_Blink_screen(arg_bool, point)

    def effect_blink_stalker_screen(self, arg_bool, point):
        """
        Function ID: 181
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_Blink_Stalker_screen(arg_bool, point)

    def effect_shadowstride_screen(self, arg_bool, point):
        """
        Function ID: 182
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_ShadowStride_screen(arg_bool, point)

    def effect_calldownmule_screen(self, arg_bool, point):
        """
        Function ID: 183
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_CalldownMULE_screen(arg_bool, point)

    def effect_causticspray_screen(self, arg_bool, point):
        """
        Function ID: 184
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_CausticSpray_screen(arg_bool, point)

    def effect_charge_screen(self, arg_bool, point):
        """
        Function ID: 185
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_Charge_screen(arg_bool, point)

    def effect_charge_autocast(self):
        """
        Function ID: 186
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_Charge_autocast()

    def effect_chronoboost_screen(self, arg_bool, point):
        """
        Function ID: 187
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_ChronoBoost_screen(arg_bool, point)

    def effect_chronoboostenergycost_screen(self, arg_bool, point):
        """
        Function ID: 527
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_ChronoBoostEnergyCost_screen(arg_bool, point)

    def effect_contaminate_screen(self, arg_bool, point):
        """
        Function ID: 188
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_Contaminate_screen(arg_bool, point)

    def effect_corrosivebile_screen(self, arg_bool, point):
        """
        Function ID: 189
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_CorrosiveBile_screen(arg_bool, point)

    def effect_emp_screen(self, arg_bool, point):
        """
        Function ID: 190
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_EMP_screen(arg_bool, point)

    def effect_explode_quick(self, arg_bool):
        """
        Function ID: 191
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_Explode_quick(arg_bool)

    def effect_feedback_screen(self, arg_bool, point):
        """
        Function ID: 192
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_Feedback_screen(arg_bool, point)

    def effect_forcefield_screen(self, arg_bool, point):
        """
        Function ID: 193
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_ForceField_screen(arg_bool, point)

    def effect_fungalgrowth_screen(self, arg_bool, point):
        """
        Function ID: 194
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_FungalGrowth_screen(arg_bool, point)

    def effect_ghostsnipe_screen(self, arg_bool, point):
        """
        Function ID: 195
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_GhostSnipe_screen(arg_bool, point)

    def effect_gravitonbeam_screen(self, arg_bool, point):
        """
        Function ID: 196
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_GravitonBeam_screen(arg_bool, point)

    def effect_guardianshield_quick(self, arg_bool):
        """
        Function ID: 197
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_GuardianShield_quick(arg_bool)

    def effect_heal_screen(self, arg_bool, point):
        """
        Function ID: 198
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_Heal_screen(arg_bool, point)

    def effect_heal_autocast(self):
        """
        Function ID: 199
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_Heal_autocast()

    def effect_hunterseekermissile_screen(self, arg_bool, point):
        """
        Function ID: 200
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_HunterSeekerMissile_screen(arg_bool, point)

    def effect_immortalbarrier_quick(self, arg_bool):
        """
        Function ID: 201
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_ImmortalBarrier_quick(arg_bool)

    def effect_immortalbarrier_autocast(self):
        """
        Function ID: 202
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_ImmortalBarrier_autocast()

    def effect_infestedterrans_screen(self, arg_bool, point):
        """
        Function ID: 203
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_InfestedTerrans_screen(arg_bool, point)

    def effect_injectlarva_screen(self, arg_bool, point):
        """
        Function ID: 204
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_InjectLarva_screen(arg_bool, point)

    def effect_interferencematrix_screen(self, arg_bool, point):
        """
        Function ID: 528
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_InterferenceMatrix_screen(arg_bool, point)

    def effect_kd8charge_screen(self, arg_bool, point):
        """
        Function ID: 205
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_KD8Charge_screen(arg_bool, point)

    def effect_lockon_screen(self, arg_bool, point):
        """
        Function ID: 206
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_LockOn_screen(arg_bool, point)

    def effect_locustswoop_screen(self, arg_bool, point):
        """
        Function ID: 207
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_LocustSwoop_screen(arg_bool, point)

    def effect_massrecall_screen(self, arg_bool, point):
        """
        Function ID: 208
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_MassRecall_screen(arg_bool, point)

    def effect_massrecall_mothership_screen(self, arg_bool, point):
        """
        Function ID: 209
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_MassRecall_Mothership_screen(arg_bool, point)

    def effect_massrecall_mothershipcore_screen(self, arg_bool, point):
        """
        Function ID: 210
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_MassRecall_MothershipCore_screen(arg_bool, point)

    def effect_massrecallnexus_screen(self, arg_bool, point):
        """
        Function ID: 529
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_MassRecallNexus_screen(arg_bool, point)

    def effect_medivacigniteafterburners_quick(self, arg_bool):
        """
        Function ID: 211
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_MedivacIgniteAfterburners_quick(arg_bool)

    def effect_neuralparasite_screen(self, arg_bool, point):
        """
        Function ID: 212
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_NeuralParasite_screen(arg_bool, point)

    def effect_nukecalldown_screen(self, arg_bool, point):
        """
        Function ID: 213
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_NukeCalldown_screen(arg_bool, point)

    def effect_oraclerevelation_screen(self, arg_bool, point):
        """
        Function ID: 214
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_OracleRevelation_screen(arg_bool, point)

    def effect_parasiticbomb_screen(self, arg_bool, point):
        """
        Function ID: 215
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_ParasiticBomb_screen(arg_bool, point)

    def effect_photonovercharge_screen(self, arg_bool, point):
        """
        Function ID: 216
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_PhotonOvercharge_screen(arg_bool, point)

    def effect_pointdefensedrone_screen(self, arg_bool, point):
        """
        Function ID: 217
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_PointDefenseDrone_screen(arg_bool, point)

    def effect_psistorm_screen(self, arg_bool, point):
        """
        Function ID: 218
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_PsiStorm_screen(arg_bool, point)

    def effect_purificationnova_screen(self, arg_bool, point):
        """
        Function ID: 219
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_PurificationNova_screen(arg_bool, point)

    def effect_repair_screen(self, arg_bool, point):
        """
        Function ID: 220
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_Repair_screen(arg_bool, point)

    def effect_repair_autocast(self):
        """
        Function ID: 221
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_Repair_autocast()

    def effect_repair_mule_screen(self, arg_bool, point):
        """
        Function ID: 222
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_Repair_Mule_screen(arg_bool, point)

    def effect_repair_mule_autocast(self):
        """
        Function ID: 223
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_Repair_Mule_autocast()

    def effect_repair_repairdrone_screen(self, arg_bool, point):
        """
        Function ID: 530
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_Repair_RepairDrone_screen(arg_bool, point)

    def effect_repair_repairdrone_autocast(self):
        """
        Function ID: 531
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_Repair_RepairDrone_autocast()

    def effect_repair_scv_screen(self, arg_bool, point):
        """
        Function ID: 224
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_Repair_SCV_screen(arg_bool, point)

    def effect_repair_scv_autocast(self):
        """
        Function ID: 225
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_Repair_SCV_autocast()

    def effect_repairdrone_screen(self, arg_bool, point):
        """
        Function ID: 532
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_RepairDrone_screen(arg_bool, point)

    def effect_restore_screen(self, arg_bool, point):
        """
        Function ID: 533
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_Restore_screen(arg_bool, point)

    def effect_restore_autocast(self):
        """
        Function ID: 534
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_Restore_autocast()

    def effect_salvage_quick(self, arg_bool):
        """
        Function ID: 226
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_Salvage_quick(arg_bool)

    def effect_scan_screen(self, arg_bool, point):
        """
        Function ID: 227
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_Scan_screen(arg_bool, point)

    def effect_spawnchangeling_quick(self, arg_bool):
        """
        Function ID: 228
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_SpawnChangeling_quick(arg_bool)

    def effect_spawnlocusts_screen(self, arg_bool, point):
        """
        Function ID: 229
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_SpawnLocusts_screen(arg_bool, point)

    def effect_spray_screen(self, arg_bool, point):
        """
        Function ID: 230
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_Spray_screen(arg_bool, point)

    def effect_spray_protoss_screen(self, arg_bool, point):
        """
        Function ID: 231
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_Spray_Protoss_screen(arg_bool, point)

    def effect_spray_terran_screen(self, arg_bool, point):
        """
        Function ID: 232
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_Spray_Terran_screen(arg_bool, point)

    def effect_spray_zerg_screen(self, arg_bool, point):
        """
        Function ID: 233
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_Spray_Zerg_screen(arg_bool, point)

    def effect_stim_quick(self, arg_bool):
        """
        Function ID: 234
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_Stim_quick(arg_bool)

    def effect_stim_marauder_quick(self, arg_bool):
        """
        Function ID: 235
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_Stim_Marauder_quick(arg_bool)

    def effect_stim_marauder_redirect_quick(self, arg_bool):
        """
        Function ID: 236
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_Stim_Marauder_Redirect_quick(arg_bool)

    def effect_stim_marine_quick(self, arg_bool):
        """
        Function ID: 237
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_Stim_Marine_quick(arg_bool)

    def effect_stim_marine_redirect_quick(self, arg_bool):
        """
        Function ID: 238
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_Stim_Marine_Redirect_quick(arg_bool)

    def effect_supplydrop_screen(self, arg_bool, point):
        """
        Function ID: 239
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_SupplyDrop_screen(arg_bool, point)

    def effect_tacticaljump_screen(self, arg_bool, point):
        """
        Function ID: 240
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_TacticalJump_screen(arg_bool, point)

    def effect_timewarp_screen(self, arg_bool, point):
        """
        Function ID: 241
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_TimeWarp_screen(arg_bool, point)

    def effect_transfusion_screen(self, arg_bool, point):
        """
        Function ID: 242
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_Transfusion_screen(arg_bool, point)

    def effect_viperconsume_screen(self, arg_bool, point):
        """
        Function ID: 243
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_ViperConsume_screen(arg_bool, point)

    def effect_voidrayprismaticalignment_quick(self, arg_bool):
        """
        Function ID: 244
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_VoidRayPrismaticAlignment_quick(arg_bool)

    def effect_widowmineattack_screen(self, arg_bool, point):
        """
        Function ID: 245
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_WidowMineAttack_screen(arg_bool, point)

    def effect_widowmineattack_autocast(self):
        """
        Function ID: 246
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_WidowMineAttack_autocast()

    def effect_yamatogun_screen(self, arg_bool, point):
        """
        Function ID: 247
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Effect_YamatoGun_screen(arg_bool, point)

    def hallucination_adept_quick(self, arg_bool):
        """
        Function ID: 248
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Hallucination_Adept_quick(arg_bool)

    def hallucination_archon_quick(self, arg_bool):
        """
        Function ID: 249
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Hallucination_Archon_quick(arg_bool)

    def hallucination_colossus_quick(self, arg_bool):
        """
        Function ID: 250
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Hallucination_Colossus_quick(arg_bool)

    def hallucination_disruptor_quick(self, arg_bool):
        """
        Function ID: 251
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Hallucination_Disruptor_quick(arg_bool)

    def hallucination_hightemplar_quick(self, arg_bool):
        """
        Function ID: 252
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Hallucination_HighTemplar_quick(arg_bool)

    def hallucination_immortal_quick(self, arg_bool):
        """
        Function ID: 253
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Hallucination_Immortal_quick(arg_bool)

    def hallucination_oracle_quick(self, arg_bool):
        """
        Function ID: 254
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Hallucination_Oracle_quick(arg_bool)

    def hallucination_phoenix_quick(self, arg_bool):
        """
        Function ID: 255
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Hallucination_Phoenix_quick(arg_bool)

    def hallucination_probe_quick(self, arg_bool):
        """
        Function ID: 256
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Hallucination_Probe_quick(arg_bool)

    def hallucination_stalker_quick(self, arg_bool):
        """
        Function ID: 257
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Hallucination_Stalker_quick(arg_bool)

    def hallucination_voidray_quick(self, arg_bool):
        """
        Function ID: 258
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Hallucination_VoidRay_quick(arg_bool)

    def hallucination_warpprism_quick(self, arg_bool):
        """
        Function ID: 259
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Hallucination_WarpPrism_quick(arg_bool)

    def hallucination_zealot_quick(self, arg_bool):
        """
        Function ID: 260
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Hallucination_Zealot_quick(arg_bool)

    def halt_quick(self, arg_bool):
        """
        Function ID: 261
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Halt_quick(arg_bool)

    def halt_building_quick(self, arg_bool):
        """
        Function ID: 262
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Halt_Building_quick(arg_bool)

    def halt_terranbuild_quick(self, arg_bool):
        """
        Function ID: 263
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Halt_TerranBuild_quick(arg_bool)

    def harvest_gather_screen(self, arg_bool, point):
        """
        Function ID: 264
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Harvest_Gather_screen(arg_bool, point)

    def harvest_gather_drone_screen(self, arg_bool, point):
        """
        Function ID: 265
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Harvest_Gather_Drone_screen(arg_bool, point)

    def harvest_gather_mule_screen(self, arg_bool, point):
        """
        Function ID: 266
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Harvest_Gather_Mule_screen(arg_bool, point)

    def harvest_gather_probe_screen(self, arg_bool, point):
        """
        Function ID: 267
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Harvest_Gather_Probe_screen(arg_bool, point)

    def harvest_gather_scv_screen(self, arg_bool, point):
        """
        Function ID: 268
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Harvest_Gather_SCV_screen(arg_bool, point)

    def harvest_return_quick(self, arg_bool):
        """
        Function ID: 269
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Harvest_Return_quick(arg_bool)

    def harvest_return_drone_quick(self, arg_bool):
        """
        Function ID: 270
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Harvest_Return_Drone_quick(arg_bool)

    def harvest_return_mule_quick(self, arg_bool):
        """
        Function ID: 271
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Harvest_Return_Mule_quick(arg_bool)

    def harvest_return_probe_quick(self, arg_bool):
        """
        Function ID: 272
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Harvest_Return_Probe_quick(arg_bool)

    def harvest_return_scv_quick(self, arg_bool):
        """
        Function ID: 273
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Harvest_Return_SCV_quick(arg_bool)

    def holdposition_quick(self, arg_bool):
        """
        Function ID: 274
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.HoldPosition_quick(arg_bool)

    def land_screen(self, arg_bool, point):
        """
        Function ID: 275
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Land_screen(arg_bool, point)

    def land_barracks_screen(self, arg_bool, point):
        """
        Function ID: 276
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Land_Barracks_screen(arg_bool, point)

    def land_commandcenter_screen(self, arg_bool, point):
        """
        Function ID: 277
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Land_CommandCenter_screen(arg_bool, point)

    def land_factory_screen(self, arg_bool, point):
        """
        Function ID: 278
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Land_Factory_screen(arg_bool, point)

    def land_orbitalcommand_screen(self, arg_bool, point):
        """
        Function ID: 279
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Land_OrbitalCommand_screen(arg_bool, point)

    def land_starport_screen(self, arg_bool, point):
        """
        Function ID: 280
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Land_Starport_screen(arg_bool, point)

    def lift_quick(self, arg_bool):
        """
        Function ID: 281
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Lift_quick(arg_bool)

    def lift_barracks_quick(self, arg_bool):
        """
        Function ID: 282
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Lift_Barracks_quick(arg_bool)

    def lift_commandcenter_quick(self, arg_bool):
        """
        Function ID: 283
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Lift_CommandCenter_quick(arg_bool)

    def lift_factory_quick(self, arg_bool):
        """
        Function ID: 284
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Lift_Factory_quick(arg_bool)

    def lift_orbitalcommand_quick(self, arg_bool):
        """
        Function ID: 285
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Lift_OrbitalCommand_quick(arg_bool)

    def lift_starport_quick(self, arg_bool):
        """
        Function ID: 286
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Lift_Starport_quick(arg_bool)

    def load_screen(self, arg_bool, point):
        """
        Function ID: 287
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Load_screen(arg_bool, point)

    def load_bunker_screen(self, arg_bool, point):
        """
        Function ID: 288
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Load_Bunker_screen(arg_bool, point)

    def load_medivac_screen(self, arg_bool, point):
        """
        Function ID: 289
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Load_Medivac_screen(arg_bool, point)

    def load_nydusnetwork_screen(self, arg_bool, point):
        """
        Function ID: 290
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Load_NydusNetwork_screen(arg_bool, point)

    def load_nydusworm_screen(self, arg_bool, point):
        """
        Function ID: 291
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Load_NydusWorm_screen(arg_bool, point)

    def load_overlord_screen(self, arg_bool, point):
        """
        Function ID: 292
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Load_Overlord_screen(arg_bool, point)

    def load_warpprism_screen(self, arg_bool, point):
        """
        Function ID: 293
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Load_WarpPrism_screen(arg_bool, point)

    def loadall_quick(self, arg_bool):
        """
        Function ID: 294
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.LoadAll_quick(arg_bool)

    def loadall_commandcenter_quick(self, arg_bool):
        """
        Function ID: 295
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.LoadAll_CommandCenter_quick(arg_bool)

    def morph_archon_quick(self, arg_bool):
        """
        Function ID: 296
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_Archon_quick(arg_bool)

    def morph_broodlord_quick(self, arg_bool):
        """
        Function ID: 297
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_BroodLord_quick(arg_bool)

    def morph_gateway_quick(self, arg_bool):
        """
        Function ID: 298
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_Gateway_quick(arg_bool)

    def morph_greaterspire_quick(self, arg_bool):
        """
        Function ID: 299
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_GreaterSpire_quick(arg_bool)

    def morph_hellbat_quick(self, arg_bool):
        """
        Function ID: 300
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_Hellbat_quick(arg_bool)

    def morph_hellion_quick(self, arg_bool):
        """
        Function ID: 301
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_Hellion_quick(arg_bool)

    def morph_hive_quick(self, arg_bool):
        """
        Function ID: 302
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_Hive_quick(arg_bool)

    def morph_lair_quick(self, arg_bool):
        """
        Function ID: 303
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_Lair_quick(arg_bool)

    def morph_liberatoraamode_quick(self, arg_bool):
        """
        Function ID: 304
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_LiberatorAAMode_quick(arg_bool)

    def morph_liberatoragmode_screen(self, arg_bool, point):
        """
        Function ID: 305
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_LiberatorAGMode_screen(arg_bool, point)

    def morph_lurker_quick(self, arg_bool):
        """
        Function ID: 306
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_Lurker_quick(arg_bool)

    def morph_lurkerden_quick(self, arg_bool):
        """
        Function ID: 307
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_LurkerDen_quick(arg_bool)

    def morph_mothership_quick(self, arg_bool):
        """
        Function ID: 308
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_Mothership_quick(arg_bool)

    def morph_observermode_quick(self, arg_bool):
        """
        Function ID: 535
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_ObserverMode_quick(arg_bool)

    def morph_orbitalcommand_quick(self, arg_bool):
        """
        Function ID: 309
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_OrbitalCommand_quick(arg_bool)

    def morph_overlordtransport_quick(self, arg_bool):
        """
        Function ID: 310
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_OverlordTransport_quick(arg_bool)

    def morph_overseer_quick(self, arg_bool):
        """
        Function ID: 311
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_Overseer_quick(arg_bool)

    def morph_overseermode_quick(self, arg_bool):
        """
        Function ID: 536
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_OverseerMode_quick(arg_bool)

    def morph_oversightmode_quick(self, arg_bool):
        """
        Function ID: 537
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_OversightMode_quick(arg_bool)

    def morph_surveillancemode_quick(self, arg_bool):
        """
        Function ID: 538
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_SurveillanceMode_quick(arg_bool)

    def morph_planetaryfortress_quick(self, arg_bool):
        """
        Function ID: 312
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_PlanetaryFortress_quick(arg_bool)

    def morph_ravager_quick(self, arg_bool):
        """
        Function ID: 313
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_Ravager_quick(arg_bool)

    def morph_root_screen(self, arg_bool, point):
        """
        Function ID: 314
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_Root_screen(arg_bool, point)

    def morph_spinecrawlerroot_screen(self, arg_bool, point):
        """
        Function ID: 315
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_SpineCrawlerRoot_screen(arg_bool, point)

    def morph_sporecrawlerroot_screen(self, arg_bool, point):
        """
        Function ID: 316
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_SporeCrawlerRoot_screen(arg_bool, point)

    def morph_siegemode_quick(self, arg_bool):
        """
        Function ID: 317
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_SiegeMode_quick(arg_bool)

    def morph_supplydepot_lower_quick(self, arg_bool):
        """
        Function ID: 318
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_SupplyDepot_Lower_quick(arg_bool)

    def morph_supplydepot_raise_quick(self, arg_bool):
        """
        Function ID: 319
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_SupplyDepot_Raise_quick(arg_bool)

    def morph_thorexplosivemode_quick(self, arg_bool):
        """
        Function ID: 320
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_ThorExplosiveMode_quick(arg_bool)

    def morph_thorhighimpactmode_quick(self, arg_bool):
        """
        Function ID: 321
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_ThorHighImpactMode_quick(arg_bool)

    def morph_unsiege_quick(self, arg_bool):
        """
        Function ID: 322
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_Unsiege_quick(arg_bool)

    def morph_uproot_quick(self, arg_bool):
        """
        Function ID: 323
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_Uproot_quick(arg_bool)

    def morph_spinecrawleruproot_quick(self, arg_bool):
        """
        Function ID: 324
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_SpineCrawlerUproot_quick(arg_bool)

    def morph_sporecrawleruproot_quick(self, arg_bool):
        """
        Function ID: 325
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_SporeCrawlerUproot_quick(arg_bool)

    def morph_vikingassaultmode_quick(self, arg_bool):
        """
        Function ID: 326
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_VikingAssaultMode_quick(arg_bool)

    def morph_vikingfightermode_quick(self, arg_bool):
        """
        Function ID: 327
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_VikingFighterMode_quick(arg_bool)

    def morph_warpgate_quick(self, arg_bool):
        """
        Function ID: 328
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_WarpGate_quick(arg_bool)

    def morph_warpprismphasingmode_quick(self, arg_bool):
        """
        Function ID: 329
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_WarpPrismPhasingMode_quick(arg_bool)

    def morph_warpprismtransportmode_quick(self, arg_bool):
        """
        Function ID: 330
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Morph_WarpPrismTransportMode_quick(arg_bool)

    def move_screen(self, arg_bool, point):
        """
        Function ID: 331
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Move_screen(arg_bool, point)

    def move_minimap(self, arg_bool, point):
        """
        Function ID: 332
        :param arg_bool:
        :param point: Point to attack on the minimap
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Move_minimap(arg_bool, point)

    def patrol_screen(self, arg_bool, point):
        """
        Function ID: 333
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Patrol_screen(arg_bool, point)

    def patrol_minimap(self, arg_bool, point):
        """
        Function ID: 334
        :param arg_bool:
        :param point: Point to attack on the minimap
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Patrol_minimap(arg_bool, point)

    def rally_units_screen(self, arg_bool, point):
        """
        Function ID: 335
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Rally_Units_screen(arg_bool, point)

    def rally_units_minimap(self, arg_bool, point):
        """
        Function ID: 336
        :param arg_bool:
        :param point: Point to attack on the minimap
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Rally_Units_minimap(arg_bool, point)

    def rally_building_screen(self, arg_bool, point):
        """
        Function ID: 337
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Rally_Building_screen(arg_bool, point)

    def rally_building_minimap(self, arg_bool, point):
        """
        Function ID: 338
        :param arg_bool:
        :param point: Point to attack on the minimap
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Rally_Building_minimap(arg_bool, point)

    def rally_hatchery_units_screen(self, arg_bool, point):
        """
        Function ID: 339
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Rally_Hatchery_Units_screen(arg_bool, point)

    def rally_hatchery_units_minimap(self, arg_bool, point):
        """
        Function ID: 340
        :param arg_bool:
        :param point: Point to attack on the minimap
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Rally_Hatchery_Units_minimap(arg_bool, point)

    def rally_morphing_unit_screen(self, arg_bool, point):
        """
        Function ID: 341
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Rally_Morphing_Unit_screen(arg_bool, point)

    def rally_morphing_unit_minimap(self, arg_bool, point):
        """
        Function ID: 342
        :param arg_bool:
        :param point: Point to attack on the minimap
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Rally_Morphing_Unit_minimap(arg_bool, point)

    def rally_workers_screen(self, arg_bool, point):
        """
        Function ID: 343
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Rally_Workers_screen(arg_bool, point)

    def rally_workers_minimap(self, arg_bool, point):
        """
        Function ID: 344
        :param arg_bool:
        :param point: Point to attack on the minimap
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Rally_Workers_minimap(arg_bool, point)

    def rally_commandcenter_screen(self, arg_bool, point):
        """
        Function ID: 345
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Rally_CommandCenter_screen(arg_bool, point)

    def rally_commandcenter_minimap(self, arg_bool, point):
        """
        Function ID: 346
        :param arg_bool:
        :param point: Point to attack on the minimap
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Rally_CommandCenter_minimap(arg_bool, point)

    def rally_hatchery_workers_screen(self, arg_bool, point):
        """
        Function ID: 347
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Rally_Hatchery_Workers_screen(arg_bool, point)

    def rally_hatchery_workers_minimap(self, arg_bool, point):
        """
        Function ID: 348
        :param arg_bool:
        :param point: Point to attack on the minimap
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Rally_Hatchery_Workers_minimap(arg_bool, point)

    def rally_nexus_screen(self, arg_bool, point):
        """
        Function ID: 349
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Rally_Nexus_screen(arg_bool, point)

    def rally_nexus_minimap(self, arg_bool, point):
        """
        Function ID: 350
        :param arg_bool:
        :param point: Point to attack on the minimap
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Rally_Nexus_minimap(arg_bool, point)

    def research_adaptivetalons_quick(self, arg_bool):
        """
        Function ID: 539
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_AdaptiveTalons_quick(arg_bool)

    def research_adeptresonatingglaives_quick(self, arg_bool):
        """
        Function ID: 351
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_AdeptResonatingGlaives_quick(arg_bool)

    def research_advancedballistics_quick(self, arg_bool):
        """
        Function ID: 352
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_AdvancedBallistics_quick(arg_bool)

    def research_bansheecloakingfield_quick(self, arg_bool):
        """
        Function ID: 353
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_BansheeCloakingField_quick(arg_bool)

    def research_bansheehyperflightrotors_quick(self, arg_bool):
        """
        Function ID: 354
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_BansheeHyperflightRotors_quick(arg_bool)

    def research_battlecruiserweaponrefit_quick(self, arg_bool):
        """
        Function ID: 355
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_BattlecruiserWeaponRefit_quick(arg_bool)

    def research_blink_quick(self, arg_bool):
        """
        Function ID: 356
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_Blink_quick(arg_bool)

    def research_burrow_quick(self, arg_bool):
        """
        Function ID: 357
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_Burrow_quick(arg_bool)

    def research_centrifugalhooks_quick(self, arg_bool):
        """
        Function ID: 358
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_CentrifugalHooks_quick(arg_bool)

    def research_charge_quick(self, arg_bool):
        """
        Function ID: 359
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_Charge_quick(arg_bool)

    def research_chitinousplating_quick(self, arg_bool):
        """
        Function ID: 360
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ChitinousPlating_quick(arg_bool)

    def research_combatshield_quick(self, arg_bool):
        """
        Function ID: 361
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_CombatShield_quick(arg_bool)

    def research_concussiveshells_quick(self, arg_bool):
        """
        Function ID: 362
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ConcussiveShells_quick(arg_bool)

    def research_cyclonerapidfirelaunchers_quick(self, arg_bool):
        """
        Function ID: 540
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_CycloneRapidFireLaunchers_quick(arg_bool)

    def research_drillingclaws_quick(self, arg_bool):
        """
        Function ID: 363
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_DrillingClaws_quick(arg_bool)

    def research_extendedthermallance_quick(self, arg_bool):
        """
        Function ID: 364
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ExtendedThermalLance_quick(arg_bool)

    def research_glialregeneration_quick(self, arg_bool):
        """
        Function ID: 365
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_GlialRegeneration_quick(arg_bool)

    def research_graviticbooster_quick(self, arg_bool):
        """
        Function ID: 366
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_GraviticBooster_quick(arg_bool)

    def research_graviticdrive_quick(self, arg_bool):
        """
        Function ID: 367
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_GraviticDrive_quick(arg_bool)

    def research_groovedspines_quick(self, arg_bool):
        """
        Function ID: 368
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_GroovedSpines_quick(arg_bool)

    def research_hisecautotracking_quick(self, arg_bool):
        """
        Function ID: 369
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_HiSecAutoTracking_quick(arg_bool)

    def research_highcapacityfueltanks_quick(self, arg_bool):
        """
        Function ID: 370
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_HighCapacityFuelTanks_quick(arg_bool)

    def research_infernalpreigniter_quick(self, arg_bool):
        """
        Function ID: 371
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_InfernalPreigniter_quick(arg_bool)

    def research_interceptorgravitoncatapult_quick(self, arg_bool):
        """
        Function ID: 372
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_InterceptorGravitonCatapult_quick(arg_bool)

    def research_muscularaugments_quick(self, arg_bool):
        """
        Function ID: 374
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_MuscularAugments_quick(arg_bool)

    def research_neosteelframe_quick(self, arg_bool):
        """
        Function ID: 375
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_NeosteelFrame_quick(arg_bool)

    def research_neuralparasite_quick(self, arg_bool):
        """
        Function ID: 376
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_NeuralParasite_quick(arg_bool)

    def research_pathogenglands_quick(self, arg_bool):
        """
        Function ID: 377
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_PathogenGlands_quick(arg_bool)

    def research_personalcloaking_quick(self, arg_bool):
        """
        Function ID: 378
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_PersonalCloaking_quick(arg_bool)

    def research_phoenixanionpulsecrystals_quick(self, arg_bool):
        """
        Function ID: 379
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_PhoenixAnionPulseCrystals_quick(arg_bool)

    def research_pneumatizedcarapace_quick(self, arg_bool):
        """
        Function ID: 380
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_PneumatizedCarapace_quick(arg_bool)

    def research_protossairarmor_quick(self, arg_bool):
        """
        Function ID: 381
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ProtossAirArmor_quick(arg_bool)

    def research_protossairarmorlevel1_quick(self, arg_bool):
        """
        Function ID: 382
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ProtossAirArmorLevel1_quick(arg_bool)

    def research_protossairarmorlevel2_quick(self, arg_bool):
        """
        Function ID: 383
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ProtossAirArmorLevel2_quick(arg_bool)

    def research_protossairarmorlevel3_quick(self, arg_bool):
        """
        Function ID: 384
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ProtossAirArmorLevel3_quick(arg_bool)

    def research_protossairweapons_quick(self, arg_bool):
        """
        Function ID: 385
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ProtossAirWeapons_quick(arg_bool)

    def research_protossairweaponslevel1_quick(self, arg_bool):
        """
        Function ID: 386
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ProtossAirWeaponsLevel1_quick(arg_bool)

    def research_protossairweaponslevel2_quick(self, arg_bool):
        """
        Function ID: 387
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ProtossAirWeaponsLevel2_quick(arg_bool)

    def research_protossairweaponslevel3_quick(self, arg_bool):
        """
        Function ID: 388
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ProtossAirWeaponsLevel3_quick(arg_bool)

    def research_protossgroundarmor_quick(self, arg_bool):
        """
        Function ID: 389
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ProtossGroundArmor_quick(arg_bool)

    def research_protossgroundarmorlevel1_quick(self, arg_bool):
        """
        Function ID: 390
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ProtossGroundArmorLevel1_quick(arg_bool)

    def research_protossgroundarmorlevel2_quick(self, arg_bool):
        """
        Function ID: 391
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ProtossGroundArmorLevel2_quick(arg_bool)

    def research_protossgroundarmorlevel3_quick(self, arg_bool):
        """
        Function ID: 392
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ProtossGroundArmorLevel3_quick(arg_bool)

    def research_protossgroundweapons_quick(self, arg_bool):
        """
        Function ID: 393
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ProtossGroundWeapons_quick(arg_bool)

    def research_protossgroundweaponslevel1_quick(self, arg_bool):
        """
        Function ID: 394
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ProtossGroundWeaponsLevel1_quick(arg_bool)

    def research_protossgroundweaponslevel2_quick(self, arg_bool):
        """
        Function ID: 395
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ProtossGroundWeaponsLevel2_quick(arg_bool)

    def research_protossgroundweaponslevel3_quick(self, arg_bool):
        """
        Function ID: 396
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ProtossGroundWeaponsLevel3_quick(arg_bool)

    def research_protossshields_quick(self, arg_bool):
        """
        Function ID: 397
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ProtossShields_quick(arg_bool)

    def research_protossshieldslevel1_quick(self, arg_bool):
        """
        Function ID: 398
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ProtossShieldsLevel1_quick(arg_bool)

    def research_protossshieldslevel2_quick(self, arg_bool):
        """
        Function ID: 399
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ProtossShieldsLevel2_quick(arg_bool)

    def research_protossshieldslevel3_quick(self, arg_bool):
        """
        Function ID: 400
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ProtossShieldsLevel3_quick(arg_bool)

    def research_psistorm_quick(self, arg_bool):
        """
        Function ID: 401
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_PsiStorm_quick(arg_bool)

    def research_ravencorvidreactor_quick(self, arg_bool):
        """
        Function ID: 402
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_RavenCorvidReactor_quick(arg_bool)

    def research_ravenrecalibratedexplosives_quick(self, arg_bool):
        """
        Function ID: 403
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_RavenRecalibratedExplosives_quick(arg_bool)

    def research_shadowstrike_quick(self, arg_bool):
        """
        Function ID: 404
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ShadowStrike_quick(arg_bool)

    def research_smartservos_quick(self, arg_bool):
        """
        Function ID: 373
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_SmartServos_quick(arg_bool)

    def research_stimpack_quick(self, arg_bool):
        """
        Function ID: 405
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_Stimpack_quick(arg_bool)

    def research_terraninfantryarmor_quick(self, arg_bool):
        """
        Function ID: 406
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_TerranInfantryArmor_quick(arg_bool)

    def research_terraninfantryarmorlevel1_quick(self, arg_bool):
        """
        Function ID: 407
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_TerranInfantryArmorLevel1_quick(arg_bool)

    def research_terraninfantryarmorlevel2_quick(self, arg_bool):
        """
        Function ID: 408
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_TerranInfantryArmorLevel2_quick(arg_bool)

    def research_terraninfantryarmorlevel3_quick(self, arg_bool):
        """
        Function ID: 409
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_TerranInfantryArmorLevel3_quick(arg_bool)

    def research_terraninfantryweapons_quick(self, arg_bool):
        """
        Function ID: 410
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_TerranInfantryWeapons_quick(arg_bool)

    def research_terraninfantryweaponslevel1_quick(self, arg_bool):
        """
        Function ID: 411
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_TerranInfantryWeaponsLevel1_quick(arg_bool)

    def research_terraninfantryweaponslevel2_quick(self, arg_bool):
        """
        Function ID: 412
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_TerranInfantryWeaponsLevel2_quick(arg_bool)

    def research_terraninfantryweaponslevel3_quick(self, arg_bool):
        """
        Function ID: 413
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_TerranInfantryWeaponsLevel3_quick(arg_bool)

    def research_terranshipweapons_quick(self, arg_bool):
        """
        Function ID: 414
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_TerranShipWeapons_quick(arg_bool)

    def research_terranshipweaponslevel1_quick(self, arg_bool):
        """
        Function ID: 415
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_TerranShipWeaponsLevel1_quick(arg_bool)

    def research_terranshipweaponslevel2_quick(self, arg_bool):
        """
        Function ID: 416
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_TerranShipWeaponsLevel2_quick(arg_bool)

    def research_terranshipweaponslevel3_quick(self, arg_bool):
        """
        Function ID: 417
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_TerranShipWeaponsLevel3_quick(arg_bool)

    def research_terranstructurearmorupgrade_quick(self, arg_bool):
        """
        Function ID: 418
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_TerranStructureArmorUpgrade_quick(arg_bool)

    def research_terranvehicleandshipplating_quick(self, arg_bool):
        """
        Function ID: 419
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_TerranVehicleAndShipPlating_quick(arg_bool)

    def research_terranvehicleandshipplatinglevel1_quick(self, arg_bool):
        """
        Function ID: 420
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_TerranVehicleAndShipPlatingLevel1_quick(arg_bool)

    def research_terranvehicleandshipplatinglevel2_quick(self, arg_bool):
        """
        Function ID: 421
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_TerranVehicleAndShipPlatingLevel2_quick(arg_bool)

    def research_terranvehicleandshipplatinglevel3_quick(self, arg_bool):
        """
        Function ID: 422
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_TerranVehicleAndShipPlatingLevel3_quick(arg_bool)

    def research_terranvehicleweapons_quick(self, arg_bool):
        """
        Function ID: 423
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_TerranVehicleWeapons_quick(arg_bool)

    def research_terranvehicleweaponslevel1_quick(self, arg_bool):
        """
        Function ID: 424
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_TerranVehicleWeaponsLevel1_quick(arg_bool)

    def research_terranvehicleweaponslevel2_quick(self, arg_bool):
        """
        Function ID: 425
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_TerranVehicleWeaponsLevel2_quick(arg_bool)

    def research_terranvehicleweaponslevel3_quick(self, arg_bool):
        """
        Function ID: 426
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_TerranVehicleWeaponsLevel3_quick(arg_bool)

    def research_tunnelingclaws_quick(self, arg_bool):
        """
        Function ID: 427
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_TunnelingClaws_quick(arg_bool)

    def research_warpgate_quick(self, arg_bool):
        """
        Function ID: 428
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_WarpGate_quick(arg_bool)

    def research_zergflyerarmor_quick(self, arg_bool):
        """
        Function ID: 429
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ZergFlyerArmor_quick(arg_bool)

    def research_zergflyerarmorlevel1_quick(self, arg_bool):
        """
        Function ID: 430
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ZergFlyerArmorLevel1_quick(arg_bool)

    def research_zergflyerarmorlevel2_quick(self, arg_bool):
        """
        Function ID: 431
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ZergFlyerArmorLevel2_quick(arg_bool)

    def research_zergflyerarmorlevel3_quick(self, arg_bool):
        """
        Function ID: 432
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ZergFlyerArmorLevel3_quick(arg_bool)

    def research_zergflyerattack_quick(self, arg_bool):
        """
        Function ID: 433
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ZergFlyerAttack_quick(arg_bool)

    def research_zergflyerattacklevel1_quick(self, arg_bool):
        """
        Function ID: 434
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ZergFlyerAttackLevel1_quick(arg_bool)

    def research_zergflyerattacklevel2_quick(self, arg_bool):
        """
        Function ID: 435
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ZergFlyerAttackLevel2_quick(arg_bool)

    def research_zergflyerattacklevel3_quick(self, arg_bool):
        """
        Function ID: 436
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ZergFlyerAttackLevel3_quick(arg_bool)

    def research_zerggroundarmor_quick(self, arg_bool):
        """
        Function ID: 437
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ZergGroundArmor_quick(arg_bool)

    def research_zerggroundarmorlevel1_quick(self, arg_bool):
        """
        Function ID: 438
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ZergGroundArmorLevel1_quick(arg_bool)

    def research_zerggroundarmorlevel2_quick(self, arg_bool):
        """
        Function ID: 439
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ZergGroundArmorLevel2_quick(arg_bool)

    def research_zerggroundarmorlevel3_quick(self, arg_bool):
        """
        Function ID: 440
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ZergGroundArmorLevel3_quick(arg_bool)

    def research_zergmeleeweapons_quick(self, arg_bool):
        """
        Function ID: 441
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ZergMeleeWeapons_quick(arg_bool)

    def research_zergmeleeweaponslevel1_quick(self, arg_bool):
        """
        Function ID: 442
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ZergMeleeWeaponsLevel1_quick(arg_bool)

    def research_zergmeleeweaponslevel2_quick(self, arg_bool):
        """
        Function ID: 443
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ZergMeleeWeaponsLevel2_quick(arg_bool)

    def research_zergmeleeweaponslevel3_quick(self, arg_bool):
        """
        Function ID: 444
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ZergMeleeWeaponsLevel3_quick(arg_bool)

    def research_zergmissileweapons_quick(self, arg_bool):
        """
        Function ID: 445
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ZergMissileWeapons_quick(arg_bool)

    def research_zergmissileweaponslevel1_quick(self, arg_bool):
        """
        Function ID: 446
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ZergMissileWeaponsLevel1_quick(arg_bool)

    def research_zergmissileweaponslevel2_quick(self, arg_bool):
        """
        Function ID: 447
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ZergMissileWeaponsLevel2_quick(arg_bool)

    def research_zergmissileweaponslevel3_quick(self, arg_bool):
        """
        Function ID: 448
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ZergMissileWeaponsLevel3_quick(arg_bool)

    def research_zerglingadrenalglands_quick(self, arg_bool):
        """
        Function ID: 449
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ZerglingAdrenalGlands_quick(arg_bool)

    def research_zerglingmetabolicboost_quick(self, arg_bool):
        """
        Function ID: 450
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Research_ZerglingMetabolicBoost_quick(arg_bool)

    def smart_screen(self, arg_bool, point):
        """
        Function ID: 451
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Smart_screen(arg_bool, point)

    def smart_minimap(self, arg_bool, point):
        """
        Function ID: 452
        :param arg_bool:
        :param point: Point to attack on the minimap
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Smart_minimap(arg_bool, point)

    def stop_quick(self, arg_bool):
        """
        Function ID: 453
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Stop_quick(arg_bool)

    def stop_building_quick(self, arg_bool):
        """
        Function ID: 454
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Stop_Building_quick(arg_bool)

    def stop_redirect_quick(self, arg_bool):
        """
        Function ID: 455
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Stop_Redirect_quick(arg_bool)

    def stop_stop_quick(self, arg_bool):
        """
        Function ID: 456
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Stop_Stop_quick(arg_bool)

    def train_adept_quick(self, arg_bool):
        """
        Function ID: 457
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Adept_quick(arg_bool)

    def train_baneling_quick(self, arg_bool):
        """
        Function ID: 458
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Baneling_quick(arg_bool)

    def train_banshee_quick(self, arg_bool):
        """
        Function ID: 459
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Banshee_quick(arg_bool)

    def train_battlecruiser_quick(self, arg_bool):
        """
        Function ID: 460
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Battlecruiser_quick(arg_bool)

    def train_carrier_quick(self, arg_bool):
        """
        Function ID: 461
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Carrier_quick(arg_bool)

    def train_colossus_quick(self, arg_bool):
        """
        Function ID: 462
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Colossus_quick(arg_bool)

    def train_corruptor_quick(self, arg_bool):
        """
        Function ID: 463
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Corruptor_quick(arg_bool)

    def train_cyclone_quick(self, arg_bool):
        """
        Function ID: 464
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Cyclone_quick(arg_bool)

    def train_darktemplar_quick(self, arg_bool):
        """
        Function ID: 465
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_DarkTemplar_quick(arg_bool)

    def train_disruptor_quick(self, arg_bool):
        """
        Function ID: 466
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Disruptor_quick(arg_bool)

    def train_drone_quick(self, arg_bool):
        """
        Function ID: 467
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Drone_quick(arg_bool)

    def train_ghost_quick(self, arg_bool):
        """
        Function ID: 468
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Ghost_quick(arg_bool)

    def train_hellbat_quick(self, arg_bool):
        """
        Function ID: 469
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Hellbat_quick(arg_bool)

    def train_hellion_quick(self, arg_bool):
        """
        Function ID: 470
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Hellion_quick(arg_bool)

    def train_hightemplar_quick(self, arg_bool):
        """
        Function ID: 471
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_HighTemplar_quick(arg_bool)

    def train_hydralisk_quick(self, arg_bool):
        """
        Function ID: 472
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Hydralisk_quick(arg_bool)

    def train_immortal_quick(self, arg_bool):
        """
        Function ID: 473
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Immortal_quick(arg_bool)

    def train_infestor_quick(self, arg_bool):
        """
        Function ID: 474
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Infestor_quick(arg_bool)

    def train_liberator_quick(self, arg_bool):
        """
        Function ID: 475
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Liberator_quick(arg_bool)

    def train_marauder_quick(self, arg_bool):
        """
        Function ID: 476
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Marauder_quick(arg_bool)

    def train_marine_quick(self, arg_bool):
        """
        Function ID: 477
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Marine_quick(arg_bool)

    def train_medivac_quick(self, arg_bool):
        """
        Function ID: 478
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Medivac_quick(arg_bool)

    def train_mothershipcore_quick(self, arg_bool):
        """
        Function ID: 479
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_MothershipCore_quick(arg_bool)

    def train_mutalisk_quick(self, arg_bool):
        """
        Function ID: 480
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Mutalisk_quick(arg_bool)

    def train_observer_quick(self, arg_bool):
        """
        Function ID: 481
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Observer_quick(arg_bool)

    def train_oracle_quick(self, arg_bool):
        """
        Function ID: 482
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Oracle_quick(arg_bool)

    def train_overlord_quick(self, arg_bool):
        """
        Function ID: 483
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Overlord_quick(arg_bool)

    def train_phoenix_quick(self, arg_bool):
        """
        Function ID: 484
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Phoenix_quick(arg_bool)

    def train_probe_quick(self, arg_bool):
        """
        Function ID: 485
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Probe_quick(arg_bool)

    def train_queen_quick(self, arg_bool):
        """
        Function ID: 486
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Queen_quick(arg_bool)

    def train_raven_quick(self, arg_bool):
        """
        Function ID: 487
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Raven_quick(arg_bool)

    def train_reaper_quick(self, arg_bool):
        """
        Function ID: 488
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Reaper_quick(arg_bool)

    def train_roach_quick(self, arg_bool):
        """
        Function ID: 489
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Roach_quick(arg_bool)

    def train_scv_quick(self, arg_bool):
        """
        Function ID: 490
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_SCV_quick(arg_bool)

    def train_sentry_quick(self, arg_bool):
        """
        Function ID: 491
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Sentry_quick(arg_bool)

    def train_siegetank_quick(self, arg_bool):
        """
        Function ID: 492
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_SiegeTank_quick(arg_bool)

    def train_stalker_quick(self, arg_bool):
        """
        Function ID: 493
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Stalker_quick(arg_bool)

    def train_swarmhost_quick(self, arg_bool):
        """
        Function ID: 494
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_SwarmHost_quick(arg_bool)

    def train_tempest_quick(self, arg_bool):
        """
        Function ID: 495
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Tempest_quick(arg_bool)

    def train_thor_quick(self, arg_bool):
        """
        Function ID: 496
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Thor_quick(arg_bool)

    def train_ultralisk_quick(self, arg_bool):
        """
        Function ID: 497
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Ultralisk_quick(arg_bool)

    def train_vikingfighter_quick(self, arg_bool):
        """
        Function ID: 498
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_VikingFighter_quick(arg_bool)

    def train_viper_quick(self, arg_bool):
        """
        Function ID: 499
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Viper_quick(arg_bool)

    def train_voidray_quick(self, arg_bool):
        """
        Function ID: 500
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_VoidRay_quick(arg_bool)

    def train_warpprism_quick(self, arg_bool):
        """
        Function ID: 501
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_WarpPrism_quick(arg_bool)

    def train_widowmine_quick(self, arg_bool):
        """
        Function ID: 502
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_WidowMine_quick(arg_bool)

    def train_zealot_quick(self, arg_bool):
        """
        Function ID: 503
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Zealot_quick(arg_bool)

    def train_zergling_quick(self, arg_bool):
        """
        Function ID: 504
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.Train_Zergling_quick(arg_bool)

    def trainwarp_adept_screen(self, arg_bool, point):
        """
        Function ID: 505
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.TrainWarp_Adept_screen(arg_bool, point)

    def trainwarp_darktemplar_screen(self, arg_bool, point):
        """
        Function ID: 506
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.TrainWarp_DarkTemplar_screen(arg_bool, point)

    def trainwarp_hightemplar_screen(self, arg_bool, point):
        """
        Function ID: 507
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.TrainWarp_HighTemplar_screen(arg_bool, point)

    def trainwarp_sentry_screen(self, arg_bool, point):
        """
        Function ID: 508
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.TrainWarp_Sentry_screen(arg_bool, point)

    def trainwarp_stalker_screen(self, arg_bool, point):
        """
        Function ID: 509
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.TrainWarp_Stalker_screen(arg_bool, point)

    def trainwarp_zealot_screen(self, arg_bool, point):
        """
        Function ID: 510
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.TrainWarp_Zealot_screen(arg_bool, point)

    def unloadall_quick(self, arg_bool):
        """
        Function ID: 511
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.UnloadAll_quick(arg_bool)

    def unloadall_bunker_quick(self, arg_bool):
        """
        Function ID: 512
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.UnloadAll_Bunker_quick(arg_bool)

    def unloadall_commandcenter_quick(self, arg_bool):
        """
        Function ID: 513
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.UnloadAll_CommandCenter_quick(arg_bool)

    def unloadall_nydasnetwork_quick(self, arg_bool):
        """
        Function ID: 514
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.UnloadAll_NydasNetwork_quick(arg_bool)

    def unloadall_nydusworm_quick(self, arg_bool):
        """
        Function ID: 515
        :param arg_bool: arg_bool: True/false value if action should be queued after other actions
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.UnloadAll_NydusWorm_quick(arg_bool)

    def unloadallat_screen(self, arg_bool, point):
        """
        Function ID: 516
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.UnloadAllAt_screen(arg_bool, point)

    def unloadallat_minimap(self, arg_bool, point):
        """
        Function ID: 517
        :param arg_bool:
        :param point: Point to attack on the minimap
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.UnloadAllAt_minimap(arg_bool, point)

    def unloadallat_medivac_screen(self, arg_bool, point):
        """
        Function ID: 518
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.UnloadAllAt_Medivac_screen(arg_bool, point)

    def unloadallat_medivac_minimap(self, arg_bool, point):
        """
        Function ID: 519
        :param arg_bool:
        :param point: Point to attack on the minimap
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.UnloadAllAt_Medivac_minimap(arg_bool, point)

    def unloadallat_overlord_screen(self, arg_bool, point):
        """
        Function ID: 520
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.UnloadAllAt_Overlord_screen(arg_bool, point)

    def unloadallat_overlord_minimap(self, arg_bool, point):
        """
        Function ID: 521
        :param arg_bool:
        :param point: Point to attack on the minimap
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.UnloadAllAt_Overlord_minimap(arg_bool, point)

    def unloadallat_warpprism_screen(self, arg_bool, point):
        """
        Function ID: 522
        :param arg_bool:
        :param point: Point to attack on the screen
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.UnloadAllAt_WarpPrism_screen(arg_bool, point)

    def unloadallat_warpprism_minimap(self, arg_bool, point):
        """
        Function ID: 523
        :param arg_bool:
        :param point: Point to attack on the minimap
        :return: the selection FunctionCall to hand to the game
        """
        return FUNCTIONS.UnloadAllAt_WarpPrism_minimap(arg_bool, point)

    def get_Primitives(self):
        # returns all possible primitves....
        list_of_primitives = []
        single_primitive = [self.do_nothing, [], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.move_camera, [genetitcTypes.PointMinimap], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.select_point, [genetitcTypes.SelectPtOpts, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.select_rect, [genetitcTypes.Bool, genetitcTypes.PointScreen,
                                               genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.select_control_group, [genetitcTypes.ControlGroupOpts, genetitcTypes.ControlCroupID],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.select_unit, [genetitcTypes.SelectUnitAction, genetitcTypes.SelectUnitID],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.select_idle_worker, [genetitcTypes.SelectWorkerOpts], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.select_marine, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.select_warp_gates, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.select_larva, [], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.unload, [genetitcTypes.UnloadID], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_queue, [genetitcTypes.BuildQueueID], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.attack_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.attack_minimap, [genetitcTypes.Bool, genetitcTypes.PointMinimap], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.attack_attack_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.attack_attack_minimap, [genetitcTypes.Bool, genetitcTypes.PointMinimap],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.attack_attackbuilding_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.attack_attackbuilding_minimap, [genetitcTypes.Bool, genetitcTypes.PointMinimap],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.attack_redirect_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.scan_move_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.scan_move_minimap, [genetitcTypes.Bool, genetitcTypes.PointMinimap], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.behavior_buildingattackoff_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.behavior_buildingattackon_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.behavior_cloakoff_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.behavior_cloakoff_banshee_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.behavior_cloakoff_ghost_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.behavior_cloakon_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.behavior_cloakon_banshee_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.behavior_cloakon_ghost_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.behavior_generatecreepoff_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.behavior_generatecreepon_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.behavior_holdfireoff_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.behavior_holdfireoff_ghost_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.behavior_holdfireoff_lurker_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.behavior_holdfireon_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.behavior_holdfireon_ghost_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.behavior_holdfireon_lurker_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.behavior_pulsarbeamoff_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.behavior_pulsarbeamon_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_armory_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_assimilator_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_banelingnest_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_barracks_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_bunker_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_commandcenter_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_creeptumor_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_creeptumor_queen_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_creeptumor_tumor_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_cyberneticscore_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_darkshrine_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_engineeringbay_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_evolutionchamber_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_extractor_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_factory_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_fleetbeacon_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_forge_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_fusioncore_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_gateway_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_ghostacademy_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_hatchery_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_hydraliskden_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_infestationpit_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_interceptors_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_interceptors_autocast, [], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_lurkerden_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_missileturret_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_nexus_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_nuke_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_nydusnetwork_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_nydusworm_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_photoncannon_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_pylon_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_reactor_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_reactor_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_reactor_barracks_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_reactor_barracks_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_reactor_factory_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_reactor_factory_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_reactor_starport_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_reactor_starport_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_refinery_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_roachwarren_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_roboticsbay_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_roboticsfacility_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_sensortower_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_shieldbattery_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_spawningpool_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_spinecrawler_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_spire_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_sporecrawler_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_stargate_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_starport_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_stasistrap_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_supplydepot_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_techlab_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_techlab_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_techlab_barracks_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_techlab_barracks_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_techlab_factory_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_techlab_factory_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_techlab_starport_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_techlab_starport_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_templararchive_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_twilightcouncil_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.build_ultraliskcavern_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowdown_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowdown_baneling_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowdown_drone_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowdown_hydralisk_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowdown_infestor_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowdown_infestorterran_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowdown_lurker_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowdown_queen_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowdown_ravager_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowdown_roach_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowdown_swarmhost_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowdown_ultralisk_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowdown_widowmine_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowdown_zergling_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowup_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowup_autocast, [], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowup_baneling_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowup_baneling_autocast, [], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowup_drone_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowup_hydralisk_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowup_hydralisk_autocast, [], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowup_infestor_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowup_infestorterran_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowup_infestorterran_autocast, [], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowup_lurker_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowup_queen_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowup_queen_autocast, [], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowup_ravager_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowup_ravager_autocast, [], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowup_roach_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowup_roach_autocast, [], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowup_swarmhost_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowup_ultralisk_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowup_ultralisk_autocast, [], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowup_widowmine_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowup_zergling_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.burrowup_zergling_autocast, [], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_adeptphaseshift_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_adeptshadephaseshift_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_barracksaddon_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_buildinprogress_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_creeptumor_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_factoryaddon_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_gravitonbeam_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_lockon_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_morphbroodlord_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_morphgreaterspire_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_morphhive_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_morphlair_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_morphlurker_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_morphlurkerden_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_morphmothership_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_morphorbital_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_morphoverlordtransport_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_morphoverseer_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_morphplanetaryfortress_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_morphravager_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_morphthorexplosivemode_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_neuralparasite_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_nuke_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_spinecrawlerroot_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_sporecrawlerroot_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_starportaddon_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_stasistrap_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_last_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_hangarqueue5_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_queue1_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_queue5_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_queueaddon_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_queuecanceltoselection_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_queuepasive_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.cancel_queuepassivecanceltoselection_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_abduct_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_adeptphaseshift_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_antiarmormissile_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_autoturret_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_blindingcloud_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_blink_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_blink_stalker_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_shadowstride_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_calldownmule_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_causticspray_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_charge_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_charge_autocast, [], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_chronoboost_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_chronoboostenergycost_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_contaminate_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_corrosivebile_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_emp_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_explode_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_feedback_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_forcefield_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_fungalgrowth_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_ghostsnipe_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_gravitonbeam_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_guardianshield_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_heal_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_heal_autocast, [], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_hunterseekermissile_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_immortalbarrier_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_immortalbarrier_autocast, [], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_infestedterrans_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_injectlarva_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_interferencematrix_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_kd8charge_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_lockon_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_locustswoop_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_massrecall_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_massrecall_mothership_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_massrecall_mothershipcore_screen, [genetitcTypes.Bool,
                                                                           genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_massrecallnexus_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_medivacigniteafterburners_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_neuralparasite_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_nukecalldown_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_oraclerevelation_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_parasiticbomb_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_photonovercharge_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_pointdefensedrone_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_psistorm_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_purificationnova_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_repair_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_repair_autocast, [], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_repair_mule_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_repair_mule_autocast, [], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_repair_repairdrone_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_repair_repairdrone_autocast, [], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_repair_scv_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_repair_scv_autocast, [], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_repairdrone_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_restore_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_restore_autocast, [], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_salvage_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_scan_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_spawnchangeling_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_spawnlocusts_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_spray_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_spray_protoss_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_spray_terran_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_spray_zerg_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_stim_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_stim_marauder_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_stim_marauder_redirect_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_stim_marine_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_stim_marine_redirect_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_supplydrop_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_tacticaljump_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_timewarp_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_transfusion_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_viperconsume_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_voidrayprismaticalignment_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_widowmineattack_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_widowmineattack_autocast, [], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.effect_yamatogun_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.hallucination_adept_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.hallucination_archon_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.hallucination_colossus_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.hallucination_disruptor_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.hallucination_hightemplar_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.hallucination_immortal_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.hallucination_oracle_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.hallucination_phoenix_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.hallucination_probe_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.hallucination_stalker_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.hallucination_voidray_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.hallucination_warpprism_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.hallucination_zealot_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.halt_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.halt_building_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.halt_terranbuild_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.harvest_gather_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.harvest_gather_drone_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.harvest_gather_mule_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.harvest_gather_probe_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.harvest_gather_scv_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.harvest_return_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.harvest_return_drone_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.harvest_return_mule_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.harvest_return_probe_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.harvest_return_scv_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.holdposition_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.land_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.land_barracks_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.land_commandcenter_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.land_factory_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.land_orbitalcommand_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.land_starport_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.lift_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.lift_barracks_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.lift_commandcenter_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.lift_factory_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.lift_orbitalcommand_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.lift_starport_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.load_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.load_bunker_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.load_medivac_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.load_nydusnetwork_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.load_nydusworm_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.load_overlord_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.load_warpprism_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.loadall_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.loadall_commandcenter_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_archon_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_broodlord_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_gateway_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_greaterspire_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_hellbat_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_hellion_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_hive_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_lair_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_liberatoraamode_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_liberatoragmode_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_lurker_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_lurkerden_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_mothership_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_observermode_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_orbitalcommand_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_overlordtransport_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_overseer_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_overseermode_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_oversightmode_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_surveillancemode_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_planetaryfortress_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_ravager_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_root_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_spinecrawlerroot_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_sporecrawlerroot_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_siegemode_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_supplydepot_lower_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_supplydepot_raise_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_thorexplosivemode_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_thorhighimpactmode_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_unsiege_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_uproot_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_spinecrawleruproot_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_sporecrawleruproot_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_vikingassaultmode_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_vikingfightermode_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_warpgate_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_warpprismphasingmode_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.morph_warpprismtransportmode_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.move_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.move_minimap, [genetitcTypes.Bool, genetitcTypes.PointMinimap], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.patrol_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.patrol_minimap, [genetitcTypes.Bool, genetitcTypes.PointMinimap], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.rally_units_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.rally_units_minimap, [genetitcTypes.Bool, genetitcTypes.PointMinimap], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.rally_building_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.rally_building_minimap, [genetitcTypes.Bool, genetitcTypes.PointMinimap],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.rally_hatchery_units_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.rally_hatchery_units_minimap, [genetitcTypes.Bool, genetitcTypes.PointMinimap],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.rally_morphing_unit_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.rally_morphing_unit_minimap, [genetitcTypes.Bool, genetitcTypes.PointMinimap],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.rally_workers_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.rally_workers_minimap, [genetitcTypes.Bool, genetitcTypes.PointMinimap],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.rally_commandcenter_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.rally_commandcenter_minimap, [genetitcTypes.Bool, genetitcTypes.PointMinimap],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.rally_hatchery_workers_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.rally_hatchery_workers_minimap, [genetitcTypes.Bool, genetitcTypes.PointMinimap],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.rally_nexus_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.rally_nexus_minimap, [genetitcTypes.Bool, genetitcTypes.PointMinimap], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_adaptivetalons_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_adeptresonatingglaives_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_advancedballistics_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_bansheecloakingfield_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_bansheehyperflightrotors_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_battlecruiserweaponrefit_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_blink_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_burrow_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_centrifugalhooks_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_charge_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_chitinousplating_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_combatshield_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_concussiveshells_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_cyclonerapidfirelaunchers_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_drillingclaws_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_extendedthermallance_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_glialregeneration_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_graviticbooster_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_graviticdrive_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_groovedspines_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_hisecautotracking_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_highcapacityfueltanks_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_infernalpreigniter_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_interceptorgravitoncatapult_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_muscularaugments_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_neosteelframe_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_neuralparasite_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_pathogenglands_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_personalcloaking_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_phoenixanionpulsecrystals_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_pneumatizedcarapace_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_protossairarmor_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_protossairarmorlevel1_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_protossairarmorlevel2_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_protossairarmorlevel3_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_protossairweapons_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_protossairweaponslevel1_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_protossairweaponslevel2_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_protossairweaponslevel3_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_protossgroundarmor_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_protossgroundarmorlevel1_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_protossgroundarmorlevel2_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_protossgroundarmorlevel3_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_protossgroundweapons_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_protossgroundweaponslevel1_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_protossgroundweaponslevel2_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_protossgroundweaponslevel3_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_protossshields_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_protossshieldslevel1_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_protossshieldslevel2_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_protossshieldslevel3_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_psistorm_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_ravencorvidreactor_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_ravenrecalibratedexplosives_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_shadowstrike_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_smartservos_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_stimpack_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_terraninfantryarmor_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_terraninfantryarmorlevel1_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_terraninfantryarmorlevel2_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_terraninfantryarmorlevel3_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_terraninfantryweapons_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_terraninfantryweaponslevel1_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_terraninfantryweaponslevel2_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_terraninfantryweaponslevel3_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_terranshipweapons_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_terranshipweaponslevel1_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_terranshipweaponslevel2_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_terranshipweaponslevel3_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_terranstructurearmorupgrade_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_terranvehicleandshipplating_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_terranvehicleandshipplatinglevel1_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_terranvehicleandshipplatinglevel2_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_terranvehicleandshipplatinglevel3_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_terranvehicleweapons_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_terranvehicleweaponslevel1_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_terranvehicleweaponslevel2_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_terranvehicleweaponslevel3_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_tunnelingclaws_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_warpgate_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_zergflyerarmor_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_zergflyerarmorlevel1_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_zergflyerarmorlevel2_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_zergflyerarmorlevel3_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_zergflyerattack_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_zergflyerattacklevel1_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_zergflyerattacklevel2_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_zergflyerattacklevel3_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_zerggroundarmor_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_zerggroundarmorlevel1_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_zerggroundarmorlevel2_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_zerggroundarmorlevel3_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_zergmeleeweapons_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_zergmeleeweaponslevel1_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_zergmeleeweaponslevel2_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_zergmeleeweaponslevel3_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_zergmissileweapons_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_zergmissileweaponslevel1_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_zergmissileweaponslevel2_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_zergmissileweaponslevel3_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_zerglingadrenalglands_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.research_zerglingmetabolicboost_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.smart_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.smart_minimap, [genetitcTypes.Bool, genetitcTypes.PointMinimap], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.stop_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.stop_building_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.stop_redirect_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.stop_stop_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_adept_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_baneling_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_banshee_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_battlecruiser_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_carrier_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_colossus_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_corruptor_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_cyclone_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_darktemplar_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_disruptor_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_drone_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_ghost_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_hellbat_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_hellion_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_hightemplar_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_hydralisk_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_immortal_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_infestor_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_liberator_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_marauder_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_marine_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_medivac_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_mothershipcore_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_mutalisk_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_observer_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_oracle_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_overlord_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_phoenix_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_probe_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_queen_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_raven_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_reaper_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_roach_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_scv_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_sentry_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_siegetank_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_stalker_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_swarmhost_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_tempest_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_thor_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_ultralisk_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_vikingfighter_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_viper_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_voidray_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_warpprism_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_widowmine_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_zealot_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.train_zergling_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.trainwarp_adept_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.trainwarp_darktemplar_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.trainwarp_hightemplar_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.trainwarp_sentry_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.trainwarp_stalker_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.trainwarp_zealot_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.unloadall_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.unloadall_bunker_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.unloadall_commandcenter_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.unloadall_nydasnetwork_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.unloadall_nydusworm_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.unloadallat_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.unloadallat_minimap, [genetitcTypes.Bool, genetitcTypes.PointMinimap], Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.unloadallat_medivac_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.unloadallat_medivac_minimap, [genetitcTypes.Bool, genetitcTypes.PointMinimap],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.unloadallat_overlord_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.unloadallat_overlord_minimap, [genetitcTypes.Bool, genetitcTypes.PointMinimap],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.unloadallat_warpprism_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen],
                            Function, None]
        list_of_primitives.append(single_primitive)
        single_primitive = [self.unloadallat_warpprism_minimap, [genetitcTypes.Bool, genetitcTypes.PointMinimap],
                            Function, None]
        list_of_primitives.append(single_primitive)
        return list_of_primitives

