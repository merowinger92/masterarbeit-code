from GeneticAgent import GeneticBaseAgent
from common import genetitcTypes
import numpy
from pysc2.lib import actions


class DefeatRoaches(GeneticBaseAgent.GeneticBaseAgent):

    def __init__(self):
        super(DefeatRoaches, self).__init__()
        self.user_func_id = {}

    def evaluate(self, args_to_evaluate):
        # evaluated will be the damage_done and the damage_taken... the fitness is then the difference between
        # damage_done and damage_taken... the only cases which are special cases: 1all marines dead -> which fitness?
        # 2. no further selection after first time all roaches killed

        run_results = args_to_evaluate["results_of_run"]
        observations = run_results["observations"]
        all_damage_done = 0
        all_damage_taken = 0
        for obs in observations:
            curr_damage_done = 0
            curr_damage_taken = 0
            if 12 in obs.observation.availale_actions: # 12 is the function ID of attack_screen
                # add up the
                curr_damage_done = obs.observation.score_by_vital.total_damage_dealt
                curr_damage_taken = obs.observation.score_by_vital.total_damage_taken
            else:
                # all Roaches are killed add damage done and damage taken together
                all_damage_done += curr_damage_done
                all_damage_taken += curr_damage_taken
                # nothing is selected
                # TODO: decide what should happen here if this case is now taken until end of game...

        fitness = all_damage_done - all_damage_taken
        return fitness

    def step(self, obs, routine, inject=None):
        action = super(DefeatRoaches, self).step(obs, routine)
        return action

    def return_primitives_and_terminals(self):
        pass

    def optimum_algorithm(self, obs):
        # super(DefeatRoaches, self).step(obs)
        hp = obs.observation.feature_screen.unit_hit_points
        hp_ratio = obs.observation.feature_screen.unit_hit_points_ratio
        dmg_dealt = obs.observation.score_by_vital.total_damage_dealt
        dmg_taken = obs.observation.score_by_vital.total_damage_taken
        if actions.FUNCTIONS.Attack_screen.id in obs.observation["available_actions"]:
            player_relative = obs.observation["feature_screen"]["player_relative"]
            roach_y, roach_x = numpy.nonzero(player_relative == genetitcTypes.PlayerRelativeIDs.ENEMY)
            if not roach_y.any():
                return self.do_nothing()
            index = numpy.argmax(roach_y)
            target = [roach_x[index], roach_y[index]]
            return self.attack_screen(genetitcTypes.Bool.FALSE, target)
        elif actions.FUNCTIONS.select_army.id in obs.observation["available_actions"]:
            return self.select_marine(genetitcTypes.Bool.FALSE)
        else:
            return self.do_nothing()
