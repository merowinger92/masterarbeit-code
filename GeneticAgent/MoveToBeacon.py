from GeneticAgent import GeneticBaseAgent
from pysc2.lib import actions
from pysc2.lib import features
import numpy as np
from common.exceptions import NotAvailableActionException
from common import genticFunctions
from pysc2.lib.actions import Function
from common import genetitcTypes
from scipy.spatial import distance
import collections
from operator import itemgetter

FUNCTIONS = actions.FUNCTIONS


def compare(x, y):
    return collections.Counter(x) == collections.Counter(y)


class MoveToBeacon(GeneticBaseAgent.GeneticBaseAgent):
    """
    this class is only for the generic methods applied for that case
    """
    __slots__ = ['user_func_id']

    def __init__(self):
        super(MoveToBeacon, self).__init__()
        self.user_func_id = {0, 1, 2, 3, 4, 7, 12, 13, 274, 331, 332, 333, 334, 453}

    def evaluate(self, args_to_evaluate):
        """
        calculate the manhattan distance between the beacon center and the center of the marine in the inital state (First
        timestep) and in the last timestep between the beacon center and the marine

        :param args_to_evaluate: dict with all observations and raw reward
        :return: fitness value for this agent
        """
        run_results = args_to_evaluate["results_of_run"]
        fitness = float("-100")
        observations = run_results["observations"]
        init_marine = self.search_value_in_matrix(observations[0].observation.feature_screen.player_relative,
                                                  genetitcTypes.PlayerRelativeIDs.SELF)
        init_marine_center = self.search_single_center_in_points(init_marine)
        if args_to_evaluate["eval_stage"] >= 0:
            added_up_fitness = 0
            curr_distance = 0  # can also be negative
            selected = False
            observations = run_results["observations"]
            # localise beacon and marine in the intial state and calculate manhattan distance distance
            init_beacon = self.search_value_in_matrix(observations[0].observation.feature_screen.player_relative,
                                                      genetitcTypes.PlayerRelativeIDs.NEUTRAL)
            init_beacon_center = self.search_single_center_in_points(init_beacon)
            initial_distance = distance.cityblock(init_marine_center, init_beacon_center)

            for i in range(1, len(observations)):
                beacon = self.search_value_in_matrix(observations[i].observation.feature_screen.player_relative,
                                                     genetitcTypes.PlayerRelativeIDs.NEUTRAL)
                beacon_center = self.search_single_center_in_points(beacon)
                marine = self.search_value_in_matrix(observations[i].observation.feature_screen.selected,
                                                     genetitcTypes.SelectedValues.SELECTED)
                if not marine:
                    # nothing selected
                    continue
                selected = True
                fitness = 0
                beacon_location_changed = False
                marine_center = self.search_single_center_in_points(marine)
                # has location of beacon changed? -> increase added_up_fitness by 1
                for tpl in beacon:
                    if not tpl in init_beacon:
                        beacon_location_changed = True

                if beacon_location_changed:
                    # beacon was reached -> increase added_up_fitness by 1
                    added_up_fitness = added_up_fitness + 1
                    # set new beacon centers
                    init_beacon = beacon
                    init_beacon_center = beacon_center
                    # localise marine and calc new initial distance
                    # init_marine = marine
                    init_marine_center = marine_center
                    initial_distance = distance.cityblock(init_marine_center, init_beacon_center)
                    # reset curr_distance
                    curr_distance = 0
                    continue
                # now new beacon is spawned....
                new_distance = distance.cityblock(marine_center, beacon_center)
                curr_distance = (initial_distance - new_distance) / initial_distance
                # if curr_distance < 0:
                #    curr_distance = 0

            added_up_fitness = added_up_fitness + curr_distance

            return added_up_fitness if (added_up_fitness > fitness and selected) else fitness

        # if it comes until here exception will be raised
        raise NotImplementedError

    def return_primitives_and_terminals(self):
        # add the StarCraft II primitives first
        list_of_primitives = []
        #if_else_then = [genticFunctions.if_then_else, [genetitcTypes.Bool, Function, Function],
        #                genetitcTypes.SC2Fct, None]
        #list_of_primitives.append(if_else_then)
        do_nothing = [self.do_nothing, [], Function, None]  # ID 0
        list_of_primitives.append(do_nothing)
        move_camera = [self.move_camera, [genetitcTypes.PointMinimap], Function, None]
        list_of_primitives.append(move_camera)  # ID 1
        select_point = [self.select_point, [genetitcTypes.SelectPtOpts, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(select_point)  # ID 2
        select_rect = [self.select_rect, [genetitcTypes.Bool, genetitcTypes.PointScreen,
                                          genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(select_rect)  # ID 3
        select_control_group = [self.select_control_group, [genetitcTypes.ControlGroupOpts,
                                                            genetitcTypes.ControlCroupID], Function, None]
        list_of_primitives.append(select_control_group)  # ID 4
        select_marine = [self.select_marine, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(select_marine)  # ID 7
        attack_screen = [self.attack_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(attack_screen)  # ID 12
        attack_minimap = [self.attack_minimap, [genetitcTypes.Bool, genetitcTypes.PointMinimap], Function, None]
        list_of_primitives.append(attack_minimap)  # ID 13
        holdposition_quick = [self.holdposition_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(holdposition_quick)  # ID 274
        move_screen = [self.move_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(move_screen)  # ID 331
        move_minimap = [self.move_minimap, [genetitcTypes.Bool, genetitcTypes.PointMinimap], Function, None]
        list_of_primitives.append(move_minimap)  # ID 332
        patrol_screen = [self.patrol_screen, [genetitcTypes.Bool, genetitcTypes.PointScreen], Function, None]
        list_of_primitives.append(patrol_screen)  # ID 333
        patrol_minimap = [self.patrol_minimap, [genetitcTypes.Bool, genetitcTypes.PointMinimap], Function, None]
        list_of_primitives.append(patrol_minimap)  # ID 334
        stop_quick = [self.stop_quick, [genetitcTypes.Bool], Function, None]
        list_of_primitives.append(stop_quick)  # ID 453

        # agent specific function primitves and terminals
        # since if_then_else is the control flow and also a returning SC2 Function add it more times
        # to the primitive pool. There are 14 SC2 Functions + the if_else_then Function. Add now higher amount of
        # if_then_else to raise the selection percentage of the choice of that terminal
        if_then_else = [genticFunctions.if_then_else, [genetitcTypes.Bool, Function, Function], Function, None]
        list_of_primitives.append(if_then_else)
        # for i in range(9):
        #     name = "if_then_else" + str(i)
        #     if_then_else = [genticFunctions.if_then_else, [genetitcTypes.Bool, Function, Function], Function, name]
        #     list_of_primitives.append(if_then_else)
        exists_value_in_matrix = [self.exists_value_in_matrix,  [np.ndarray, int], genetitcTypes.Bool, None]
        list_of_primitives.append(exists_value_in_matrix)
        search_value_in_matrix = [self.search_value_in_matrix, [np.ndarray, int], list, None]
        list_of_primitives.append(search_value_in_matrix)
        search_single_center_in_points = [self.search_single_center_in_points, [list], genetitcTypes.Point, None]
        list_of_primitives.append(search_single_center_in_points)
        select_min_length_max_height_point_from_points = [self.select_min_length_max_height_point_from_points, [list],
                                                          genetitcTypes.Point, None]
        list_of_primitives.append(select_min_length_max_height_point_from_points)
        select_max_length_min_height_point_from_points = [self.select_max_length_min_height_point_from_points, [list],
                                                          genetitcTypes.Point, None]
        list_of_primitives.append(select_max_length_min_height_point_from_points)



        # helper functions
        point_to_point_screen = [genticFunctions.generic_helper_primitive, [genetitcTypes.Point],
                                 genetitcTypes.PointScreen, None]
        list_of_primitives.append(point_to_point_screen)
        point_to_point_minimap = [genticFunctions.generic_helper_primitive, [genetitcTypes.Point],
                                  genetitcTypes.PointMinimap, None]
        list_of_primitives.append(point_to_point_minimap)
        list_to_list = [genticFunctions.generic_helper_primitive, [list], list, None]
        list_of_primitives.append(list_to_list)
        ndarray_to_ndarray = [genticFunctions.generic_helper_primitive, [np.ndarray], np.ndarray, None]
        list_of_primitives.append(ndarray_to_ndarray)
        int_to_int = [genticFunctions.generic_helper_primitive, [int], int, None]
        list_of_primitives.append(int_to_int)
        ctrl_grp_opt_to_ctrl_grp_opt = [genticFunctions.generic_helper_primitive, [genetitcTypes.ControlGroupOpts],
                                        genetitcTypes.ControlGroupOpts, None]
        list_of_primitives.append(ctrl_grp_opt_to_ctrl_grp_opt)
        ctrl_grp_id_to_ctrl_grp_id = [genticFunctions.generic_helper_primitive, [genetitcTypes.ControlCroupID],
                                      genetitcTypes.ControlCroupID, None]
        list_of_primitives.append(ctrl_grp_id_to_ctrl_grp_id)
        bool_to_bool = [genticFunctions.generic_helper_primitive, [genetitcTypes.Bool], genetitcTypes.Bool, None]
        list_of_primitives.append(bool_to_bool)
        sel_pt_opt_to_sel_pt_opt = [genticFunctions.generic_helper_primitive, [genetitcTypes.SelectPtOpts],
                                    genetitcTypes.SelectPtOpts, None]
        list_of_primitives.append(sel_pt_opt_to_sel_pt_opt)
        selected_values_to_int = [genticFunctions.generic_helper_primitive, [genetitcTypes.SelectedValues],
                                  int, None]
        list_of_primitives.append(selected_values_to_int)
        selected_values_to_selected_values = [genticFunctions.generic_helper_primitive, [genetitcTypes.SelectedValues],
                                              genetitcTypes.SelectedValues, None]
        list_of_primitives.append(selected_values_to_selected_values)
        player_relative_ids_to_int = [genticFunctions.generic_helper_primitive, [genetitcTypes.PlayerRelativeIDs],
                                      int, None]
        list_of_primitives.append(player_relative_ids_to_int)
        player_relative_ids_to_player_relative_ids = [genticFunctions.generic_helper_primitive,
                                                      [genetitcTypes.PlayerRelativeIDs],
                                                      genetitcTypes.PlayerRelativeIDs, None]
        list_of_primitives.append(player_relative_ids_to_player_relative_ids)

        # terminals
        # point to middle of the screen
        point_screen = [[],  [], genetitcTypes.PointScreen, None]
        list_of_primitives.append(point_screen)
        point_minimap = [[],  [], genetitcTypes.PointMinimap, None]
        list_of_primitives.append(point_minimap)
        point_list = [[],  [], list, None]
        list_of_primitives.append(point_list)
        point = [[], [], genetitcTypes.Point, None]
        list_of_primitives.append(point)

        helper_int = [-1, [], int, None]
        list_of_primitives.append(helper_int)

        helper_ndarry = [genticFunctions.helper_ndarray, [], np.ndarray, None]
        list_of_primitives.append(helper_ndarry)

        player_relative_ids = genetitcTypes.PlayerRelativeIDs.return_terminals()
        list_of_primitives.extend(player_relative_ids)

        control_group_opts = genetitcTypes.ControlGroupOpts.return_terminals()
        list_of_primitives.extend(control_group_opts)

        control_group_id = genetitcTypes.ControlCroupID.return_terminals()
        list_of_primitives.extend(control_group_id)

        sc_bool = genetitcTypes.Bool.return_terminals()
        list_of_primitives.extend(sc_bool)

        select_pt_opts = genetitcTypes.SelectPtOpts.return_terminals()
        list_of_primitives.extend(select_pt_opts)

        selected_values = genetitcTypes.SelectedValues.return_terminals()
        list_of_primitives.extend(selected_values)

        return list_of_primitives

    # # # step # # #
    def step(self, obs, routine, inject=None):
        action = super(MoveToBeacon, self).step(obs, routine)
        observation = obs.observation
        feature_screen = observation.feature_screen
        player_relative = feature_screen.player_relative
        selected_player = feature_screen.selected

        if inject is not None:
            if np.count_nonzero(selected_player) > 0:
                action = routine(player_relative, selected_player)
            else:
                action = inject(player_relative, selected_player)
        else:
            action = routine(player_relative, selected_player)

        if action.function not in obs.observation.available_actions:
            raise NotAvailableActionException("Function not available...")
        return action

    # # # step end # # #

    def check_if_agent_is_selected(self, matrix_feature_screen_selected):
        # matrix_feature_screen_selected should be this: obs.observation.feature_screen.selected should be
        # checks if anything is selected on the minimap
        if np.count_nonzero(matrix_feature_screen_selected) > 0:
            return True
        return False

    def if_agent_selected_do_else(self, matrix_feature_screen_selected, true_func, false_func):
        # matrix_feature_screen_selected should be this: obs.observation.feature_screen.selected should be
        if self.check_if_agent_is_selected(matrix_feature_screen_selected):
            true_func()
        else:
            false_func()

    def is_beacon_near(self, matrix_feature_screen_relative):
        # matrix_feature_screen_relative should be in optimal case = obs.observation.feature_screen.player_relative
        return matrix_feature_screen_relative == features.PlayerRelative.NEUTRAL

    def get_position_of_beacon(self, beacon_to_locate):
        # ideally player_to_locate is the output of is_beacon_near, which is an array(matrix) of bools containg true
        # where a player is and false where is emtpy
        y, x = np.nonzero(beacon_to_locate)
        if len(y) > 0 and len(x) > 0:
            close_player = list(zip(x, y))
            center_of_close_player = np.mean(close_player, axis=0).round()
            return center_of_close_player
        # return empty list
        return [0, 0]

    # ########matrix operations#########

    def search_single_center_in_points(self, list_of_points):
        """

        :param list_of_points: list of [x,y]
        :return: single [x,y] point (as ndarray)... Middle point of them....
        """
        value = np.mean(list_of_points, axis=0).round()
        if isinstance(value, np.ndarray):
            value = value.tolist()
        if isinstance(value, list):
            return value
        raise ValueError("Calculated Value is not a list")

    def select_min_length_max_height_point_from_points(self, list_of_points):
        y_max = max(list_of_points,key=itemgetter(1))[1]
        x_min = min(list_of_points,key=itemgetter(1))[0]
        return [x_min, y_max]

    def select_max_length_min_height_point_from_points(self, list_of_points):
        y_min = min(list_of_points, key=itemgetter(1))[1]
        x_max = min(list_of_points, key=itemgetter(1))[0]
        return [x_max, y_min]

    # ########optimum algorithm comes here####
    def optimum_algorithm(self, obs):
        player_relative = obs.observation.feature_screen.player_relative
        selected_player = obs.observation.feature_screen.selected

        result = genticFunctions.if_then_else(
            self.exists_value_in_matrix(selected_player,
                                        genticFunctions.generic_helper_primitive(
                                            genetitcTypes.SelectedValues.SELECTED)),
            genticFunctions.if_then_else(self.exists_value_in_matrix(player_relative,
                                                                     genticFunctions.generic_helper_primitive(
                                                                         genetitcTypes.PlayerRelativeIDs.NEUTRAL)),
                                         self.move_screen(genetitcTypes.Bool.TRUE, self.search_single_center_in_points(
                                             self.search_value_in_matrix(player_relative,
                                                                         genticFunctions.generic_helper_primitive(
                                                                            genetitcTypes.PlayerRelativeIDs.NEUTRAL)))),
                                         self.do_nothing()),
            self.select_rect(genetitcTypes.Bool.FALSE, self.select_min_length_max_height_point_from_points(
                self.search_value_in_matrix(player_relative, genetitcTypes.PlayerRelativeIDs.SELF)),
                             self.select_max_length_min_height_point_from_points(
                                 self.search_value_in_matrix(player_relative, genetitcTypes.PlayerRelativeIDs.SELF)))
        )

        if result.function not in obs.observation.available_actions:
            raise NotAvailableActionException("Function not available...")
        return result
