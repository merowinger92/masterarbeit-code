import random
import importlib
import numpy
import os

from absl import app
from absl import flags

from pysc2.lib import point_flag
from pysc2.lib import protocol
from pysc2.lib import remote_controller

# from deap import algorithms
from deapAlgorithms import eaSimpleModified as algorithms
from deap import base
from deap import creator
from deap import tools
from deap import gp

from common import genetitcTypes, genticFunctions
import GeneticAgent.MoveToBeacon
from Runner import run_loop
from log import logger
from pysc2.lib import actions
from pysc2.lib.actions import Function

from pysc2.lib.actions import Function
import numpy as np
import multiprocessing as mp
from multiprocessing import Queue
from common.exceptions import NotAvailableActionException

from common import config
from common import utils
from common import process_utilities as ps_utils

FUNCTIONS = actions.FUNCTIONS

# FLAGS = flags.FLAGS
# flags.DEFINE_bool("render", False, "Whether to render with pygame.")
# point_flag.DEFINE_point("feature_screen_size", "84",
#                         "Resolution for screen feature layers.")
# point_flag.DEFINE_point("feature_minimap_size", "64",
#                         "Resolution for minimap feature layers.")
# point_flag.DEFINE_point("rgb_screen_size", None,
#                         "Resolution for rendered screen.")
# point_flag.DEFINE_point("rgb_minimap_size", None,
#                         "Resolution for rendered minimap.")
# flags.DEFINE_enum("action_space", None, sc2_env.ActionSpace._member_names_,  # pylint: disable=protected-access
#                   "Which action space to use. Needed if you take both feature "
#                   "and rgb observations.")
# flags.DEFINE_bool("use_feature_units", True,
#                   "Whether to include feature units.")
# flags.DEFINE_bool("disable_fog", False, "Whether to disable Fog of War.")
#
# flags.DEFINE_integer("max_agent_steps", 0, "Total agent steps.")
# flags.DEFINE_integer("game_steps_per_episode", None, "Game steps per episode.")
# flags.DEFINE_integer("max_episodes", 0, "Total episodes.")
# flags.DEFINE_integer("step_mul", 8, "Game steps per agent step.")
#
# flags.DEFINE_string("agent", "GeneticAgent.MoveToBeacon.MoveToBeacon",
#                     "Which agent to run, as a python path to an Agent class.")
# flags.DEFINE_enum("agent_race", "random", sc2_env.Race._member_names_,  # pylint: disable=protected-access
#                   "Agent 1's race.")
#
# flags.DEFINE_string("agent2", "Bot", "Second agent, either Bot or agent class.")
# flags.DEFINE_enum("agent2_race", "random", sc2_env.Race._member_names_,  # pylint: disable=protected-access
#                   "Agent 2's race.")
# flags.DEFINE_enum("difficulty", "very_easy", sc2_env.Difficulty._member_names_,  # pylint: disable=protected-access
#                   "If agent2 is a built-in Bot, it's strength.")
#
# flags.DEFINE_bool("profile", False, "Whether to turn on code profiling.")
# flags.DEFINE_bool("trace", False, "Whether to trace the code execution.")
# flags.DEFINE_integer("parallel", 1, "How many instances to run in parallel.")
#
# flags.DEFINE_bool("save_replay", True, "Whether to save a replay at the end.")
# flags.DEFINE_string("map", "MoveToBeacon", "Name of a map to use.")

cx_prob = config.gp_run_parameters["crossover_probability"]
mut_prob = config.gp_run_parameters["mutation_probability"]
generations = config.gp_run_parameters["generations"]
population_size = config.gp_run_parameters["population_size"]
tournament_size = config.gp_run_parameters["tournament_size"]
min_depth_tree = config.gp_run_parameters["minimum_depth_tree"]
max_depth_tree = config.gp_run_parameters["maximum_depth_tree"]
min_depth_mut = config.gp_run_parameters["minimum_depth_mutation"]
max_depth_mut = config.gp_run_parameters["maximum_depth_mutation"]
random_seed = config.gp_run_parameters["random_seed"]

# all global variables
GLOBAL = utils.Globals()

ant = GeneticAgent.MoveToBeacon.MoveToBeacon()  # this call needs to be dynamic for other Bots TODO:

# pset = gp.PrimitiveSetTyped("MAIN", [np.ndarray, np.ndarray], genetitcTypes.SC2Fct, "IN")
pset = gp.PrimitiveSetTyped("MAIN", [np.ndarray, np.ndarray], Function, "IN")
pset.renameArguments(ARG0="player_relative")
pset.renameArguments(ARG1="selected")

primitive_list = ant.return_primitives_and_terminals()
for primitve in primitive_list:
    if len(primitve) > 3:  # primitive
        if len(primitve[1]) > 0:
            pset.addPrimitive(primitve[0], primitve[1], primitve[2], primitve[3])
        else:
            pset.addTerminal(primitve[0], primitve[2], primitve[3])
    else:  # terminal
        pset.addTerminal(primitve[0], primitve[1], primitve[2])

creator.create("FitnessMax", base.Fitness, weights=(1.0,))  # positive weight =>max/ negative weight=>min
creator.create("Individual", gp.PrimitiveTree, fitness=creator.FitnessMax)

toolbox = base.Toolbox()

# Attribute generator
toolbox.register("expr_init", gp.genHalfAndHalf, pset=pset, min_=min_depth_tree, max_=max_depth_tree)

# Structure initializers
toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.expr_init)
toolbox.register("population", tools.initRepeat, list, toolbox.individual)

man = mp.Manager()
GLOBAL.all_seen_funcs = man.list()


def evalArtificialAnt(individual, out_q=None, eval_stage=0, inject_str=None, inject_pset=None):
    # create env to work with
    # set environmentParameters
    from pysc2.env import sc2_env
    from pysc2.env import available_actions_printer

    players = list()
    players.append(sc2_env.Agent(sc2_env.Race["random"]))  # just add player with random race...

    env = sc2_env.SC2Env(map_name=config.sc2_params.get("SC2_map"),
                         players=players,
                         agent_interface_format=sc2_env.parse_agent_interface_format(
                             feature_screen=config.sc2_params.get("SC2_screen_size"),
                             feature_minimap=config.sc2_params.get("SC2_minimap_size"),
                             rgb_screen=config.sc2_params.get("SC2_rgb_screen"),
                             rgb_minimap=config.sc2_params.get("SC2_rgb_minimap"),
                             action_space=config.sc2_params.get("SC2_action_space"),
                             use_feature_units=config.sc2_params.get("SC2_use_feature_units")),
                         step_mul=config.sc2_params.get("SC2_step_mul"),
                         game_steps_per_episode=config.sc2_params.get("SC2_game_steps_per_episode"),
                         disable_fog=config.sc2_params.get("SC2_disable_fog"),
                         visualize=config.sc2_params.get("SC2_visualize"))
    env = available_actions_printer.AvailableActionsPrinter(env)

    # decide whether individial is an indivual (debug run) or a Queue (big run
    if isinstance(individual, mp.queues.JoinableQueue):
        while True:
            # force garbage collection to free resources
            import gc
            gc.collect()

            entry = individual.get()
            if isinstance(entry, ps_utils.PoisonPill):
                # signal the process to finish
                print(individual.qsize())
                break
            # do da evaluation
            routine = gp.compile(entry[1], pset)

            fitness = float("-100")

            inject_algo = None
            if inject_str is not None:
                inject_algo = gp.compile(inject_str, inject_pset)

            eval_stage_counter = "eval_stage-" + str(eval_stage)

            not_finished = True
            while not_finished:
                try:
                    agents = list()
                    agents.append(ant)
                    results = run_loop.run(agents, env, routine, inject_algo)
                    evaluation_args = {
                        "results_of_run": results,
                        "eval_stage": eval_stage
                    }
                    fitness = ant.evaluate(evaluation_args)

                    not_finished = False
                    logger.log_exception_and_success("success", entry, eval_stage_counter)
                except remote_controller.ConnectError:
                    print("ConnectError Exception occured!")
                    # close env and open new one
                    env.close()

                    # create new env
                    env = sc2_env.SC2Env(map_name=config.sc2_params.get("SC2_map"),
                                         players=players,
                                         agent_interface_format=sc2_env.parse_agent_interface_format(
                                             feature_screen=config.sc2_params.get("SC2_screen_size"),
                                             feature_minimap=config.sc2_params.get("SC2_minimap_size"),
                                             rgb_screen=config.sc2_params.get("SC2_rgb_screen"),
                                             rgb_minimap=config.sc2_params.get("SC2_rgb_minimap"),
                                             action_space=config.sc2_params.get("SC2_action_space"),
                                             use_feature_units=config.sc2_params.get("SC2_use_feature_units")),
                                         step_mul=config.sc2_params.get("SC2_step_mul"),
                                         game_steps_per_episode=config.sc2_params.get("SC2_game_steps_per_episode"),
                                         disable_fog=config.sc2_params.get("SC2_disable_fog"),
                                         visualize=config.sc2_params.get("SC2_visualize"))
                    env = available_actions_printer.AvailableActionsPrinter(env)
                    pass
                except protocol.ConnectionError:
                    print("ConnectionError Exception occured!")
                    # close env and open new one
                    env.close()

                    # create new env
                    env = sc2_env.SC2Env(map_name=config.sc2_params.get("SC2_map"),
                                         players=players,
                                         agent_interface_format=sc2_env.parse_agent_interface_format(
                                             feature_screen=config.sc2_params.get("SC2_screen_size"),
                                             feature_minimap=config.sc2_params.get("SC2_minimap_size"),
                                             rgb_screen=config.sc2_params.get("SC2_rgb_screen"),
                                             rgb_minimap=config.sc2_params.get("SC2_rgb_minimap"),
                                             action_space=config.sc2_params.get("SC2_action_space"),
                                             use_feature_units=config.sc2_params.get("SC2_use_feature_units")),
                                         step_mul=config.sc2_params.get("SC2_step_mul"),
                                         game_steps_per_episode=config.sc2_params.get("SC2_game_steps_per_episode"),
                                         disable_fog=config.sc2_params.get("SC2_disable_fog"),
                                         visualize=config.sc2_params.get("SC2_visualize"))
                    env = available_actions_printer.AvailableActionsPrinter(env)
                    pass
                except protocol.ProtocolError:
                    print("ProtocolError: Exception occured")
                    # close env and open new one
                    env.close()

                    # create new env
                    env = sc2_env.SC2Env(map_name=config.sc2_params.get("SC2_map"),
                                         players=players,
                                         agent_interface_format=sc2_env.parse_agent_interface_format(
                                             feature_screen=config.sc2_params.get("SC2_screen_size"),
                                             feature_minimap=config.sc2_params.get("SC2_minimap_size"),
                                             rgb_screen=config.sc2_params.get("SC2_rgb_screen"),
                                             rgb_minimap=config.sc2_params.get("SC2_rgb_minimap"),
                                             action_space=config.sc2_params.get("SC2_action_space"),
                                             use_feature_units=config.sc2_params.get("SC2_use_feature_units")),
                                         step_mul=config.sc2_params.get("SC2_step_mul"),
                                         game_steps_per_episode=config.sc2_params.get("SC2_game_steps_per_episode"),
                                         disable_fog=config.sc2_params.get("SC2_disable_fog"),
                                         visualize=config.sc2_params.get("SC2_visualize"))
                    env = available_actions_printer.AvailableActionsPrinter(env)
                    pass
                except NotAvailableActionException:
                    # Exception is raised when the algorithm returnes an unavailable action
                    print("NotAvailableActionException: ", entry[1])
                    fitness = float("-100")
                    # found
                    not_finished = False  # no second try with this individual
                    pass
                except NotImplementedError:
                    fitness = float("-50000")
                    # found
                    not_finished = False  # no second try with this individual
                    pass
                except KeyboardInterrupt:
                    print("KeyboardInterrupt")
                    fitness = float("-100")
                    not_finished = False  # no second try with this individual
                    pass
                except Exception as exception:  # all other exceptions are caught here
                    print("Exception: ", repr(exception), " individual: ", entry[1])
                    logger.log_exception_and_success(repr(exception), entry[1], eval_stage_counter)
                    fitness = float("-1000")
                    not_finished = False  # no second try with this individual
                    pass

            for func in env._seen:
                # global all_seen_funcs
                if func not in GLOBAL.all_seen_funcs:
                    GLOBAL.all_seen_funcs.append(func)

            # write back da result
            entry[1].fitness.values = (fitness,)
            out_q.append(entry)
            # say that task is ready
            individual.task_done()
            del routine
            del entry
            # force garbage collection to free resources
            gc.collect()  # don't care about stuff that would be garbage collected properly

        # exit thread
        env.close()
        individual.task_done()
    else:
        routine = gp.compile(individual, pset)

        fitness = float("-100")

        inject_algo = None
        if inject_str is not None:
            inject_algo = gp.compile(inject_str, inject_pset)

        eval_stage_counter = "eval_stage-" + str(eval_stage)

        not_finished = True
        while not_finished:
            try:
                agents = list()
                agents.append(ant)
                # global INJECT
                results = run_loop.run(agents, env, routine, inject_algo)
                # global EVAL_STAGE
                evaluation_args = {
                    "results_of_run": results,
                    "eval_stage": eval_stage
                }
                fitness = ant.evaluate(evaluation_args)

                not_finished = False
                logger.log_exception_and_success("success", individual, eval_stage_counter)
            except remote_controller.ConnectError:
                print("ConnectError Exception occured!")
                # close env and open new one
                env.close()
                pass
            except protocol.ConnectionError:
                print("ConnectionError Exception occured!")
                # close env and open new one
                env.close()
                pass
            except protocol.ProtocolError:
                print("ProtocolError: Exception occured")
                # close env and open new one
                env.close()
                pass
            except NotAvailableActionException:
                # Exception is raised when the algorithm returnes an unavailable action
                print("NotAvailableActionException: ", individual)
                fitness = float("-100")
                # found
                not_finished = False  # no second try with this individual
                pass
            except NotImplementedError:
                fitness = float("-50000")
                # found
                not_finished = False  # no second try with this individual
                pass
            except KeyboardInterrupt:
                print("KeyboardInterrupt")
                fitness = float("-100")
                not_finished = False  # no second try with this individual
                pass
            except Exception as exception:  # all other exceptions are caught here
                print("Exception: ", repr(exception), " individual: ", individual)
                logger.log_exception_and_success(repr(exception), individual, eval_stage_counter)
                fitness = float("-1000")
                not_finished = False  # no second try with this individual
                pass

        for func in env._seen:
            # global all_seen_funcs
            if func not in GLOBAL.all_seen_funcs:
                GLOBAL.all_seen_funcs.append(func)
        return fitness,


toolbox.register("evaluate", evalArtificialAnt)
toolbox.register("select", tools.selTournament, tournsize=tournament_size)
toolbox.register("mate", gp.cxOnePoint)
toolbox.register("expr_mut", gp.genHalfAndHalf, min_=min_depth_mut, max_=max_depth_mut)
toolbox.register("mutate", gp.mutUniform, expr=toolbox.expr_mut, pset=pset)


def main(unused_args):
    for i in range(10):
        random.seed(random_seed)
        # create log folder
        logger.set_and_create_log_folder(config.sc2_params.get("SC2_map") + "-Testrun")
        # log GP Run Parameters
        logger.log_gp_parameters()

        pop = toolbox.population(n=population_size)
        hof = tools.HallOfFame(1)
        stats = tools.Statistics(lambda ind: ind.fitness.values)
        stats.register("avg", numpy.mean)
        stats.register("std", numpy.std)
        stats.register("min", numpy.min)
        stats.register("max", numpy.max)

        # here only first condition should be evaluated... (reach the selected state)
        algorithms.eaSimple(pop, toolbox, cx_prob, mut_prob, generations, stats, halloffame=hof)
        logger.log_best_individual(hof[0])

        print("Best Individual: ", hof[0])
        all_seen_funcs_set = set(GLOBAL.all_seen_funcs)
        if len(all_seen_funcs_set) != len(ant.user_func_id):
            logger.log_function_discrepancies(all_seen_funcs_set, ant.user_func_id)

        # force garbage collection to free resources
        import gc
        gc.collect()

    # return pop, hof, stats


if __name__ == "__main__":
    app.run(main)
