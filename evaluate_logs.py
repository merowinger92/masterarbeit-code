import os
import glob
import time
from deap.tools import Statistics
from deap.tools import Logbook
import numpy
import pandas
import matplotlib.pyplot as plt

folders_to_read = [
    "MoveToBeacon-100-2-6",
    "MoveToBeacon-100-2-10",
    "MoveToBeacon-100-10-15",
    "MoveToBeacon-300-2-6",
    "MoveToBeacon-300-2-10",
    "MoveToBeacon-300-10-15",
    "MoveToBeacon-500-2-6",
    "MoveToBeacon-500-2-10",
    "MoveToBeacon-500-10-15",
    "MoveToBeacon-1000-2-6",
    "MoveToBeacon-1000-2-10",
    "MoveToBeacon-1000-10-15",
    "MoveToBeacon-100-2-6-t3",
    "MoveToBeacon-100-2-10-t3",
    "MoveToBeacon-100-10-15-t3",
    "MoveToBeacon-300-2-6-t3",
    "MoveToBeacon-300-2-10-t3",
    "MoveToBeacon-300-10-15-t3",
    "MoveToBeacon-500-2-6-t3",
    "MoveToBeacon-500-2-10-t3",
    # no results so far for: MoveToBeacon-500-10-15-t3, MoveToBeacon-1000-2-6-t3,
    # MoveToBeacon-1000-2-10-t3, MoveToBeacon-1000-10-15-t3
]

template_run_results = "run-*"  # results of runs are safed in folders where a run can identified with this pattern


def valid_runs(data):
    # a run is considered to be valid when the fitness value is over -100
    # data is a an array of values [1,2,3,4,5]
    valid_runs = sum(i > -100 for i in data)
    runs = len(data)
    val_run_percentage = round(valid_runs/runs, 2)*100
    return val_run_percentage


if __name__ == "__main__":
    start = time.time()
    # register statistics stuff
    stats = Statistics()
    stats.register("average", numpy.mean)
    stats.register("minimum", numpy.min)
    stats.register("maximum", numpy.max)
    stats.register("valid", valid_runs)

    # result folders where more single results are stored
    for folder in folders_to_read:
        run_folders = [f for f in os.listdir(os.path.join("logs", folder)) if
                       os.path.isdir(os.path.join("logs", folder, f))]
        # uncomment the lines to make the code work in a full run....
        # evaluate each of the runs stored in the folder
        # for run_folder in run_folders:
        #     regex = os.path.join("logs", folder, run_folder, template_run_results)
        #     single_runs = glob.glob(regex)
        #     df = pandas.DataFrame()
        #     # evaluate each generation
        #     for run in single_runs:
        #         run_dash = run.rfind("-") + 1
        #         run_dot = run.rfind(".")
        #         run_id = run[run_dash:run_dot]
        #         reader = pandas.read_csv(run, delimiter=';', header=0)
        #         fit_vals = reader[' Fitness'].tolist()
        #         record = stats.compile(fit_vals)
        #         record['ageneration'] = int(run_id)
        #         df = df.append(record, ignore_index=True)
        #     df = df.sort_values(by=['ageneration'])
        #     new_file = os.path.join("logs", folder, run_folder, "evaluation.csv")
        #     df.to_csv(new_file, index=False, sep=';')

        ###
        # if the program comes here al single runs are evaluated
        ###
        # read in all evaluation.csv
        df_list = list()
        for run_folder in run_folders:

            eval_file = os.path.join("logs", folder, run_folder, "evaluation.csv")
            df = pandas.read_csv(eval_file, delimiter=';', header=0,  index_col=False)
            df = df.set_index("ageneration", drop=True)
            df_list.append(df)
        df = pandas.DataFrame()
        for i in range(51): #  51 because the 0 is included so that it is from 0 to 50
            maximum_values = list()
            minimum_values = list()
            average_values = list()
            valid_values = list()
            for df1 in df_list:
                max_val = df1.loc[i, "maximum"]
                maximum_values.append(max_val)
                min_val = df1.loc[i, "minimum"]
                minimum_values.append(min_val)
                avg_val = df1.loc[i, "average"]
                average_values.append(avg_val)
                val_val = df1.loc[i, "valid"]
                valid_values.append(val_val)

            max_val_avg = numpy.mean(maximum_values)
            min_val_avg = numpy.mean(minimum_values)
            avg_val_avg = numpy.mean(average_values)
            val_val_avg = numpy.mean(valid_values)

            max_val_min = numpy.min(maximum_values)
            min_val_min = numpy.min(minimum_values)
            avg_val_min = numpy.min(average_values)
            val_val_min = numpy.min(valid_values)

            max_val_max = numpy.max(maximum_values)
            min_val_max = numpy.max(minimum_values)
            avg_val_max = numpy.max(average_values)
            val_val_max = numpy.max(valid_values)

            result_dict = {
                "ageneration": i,

                "maximum_avg": max_val_avg,
                "minimum_avg": min_val_avg,
                "average_avg": avg_val_avg,
                "valid_avg": val_val_avg,

                "maximum_min": max_val_min,
                "minimum_min": min_val_min,
                "average_min": avg_val_min,
                "valid_min": val_val_min,

                "maxmimum_max": max_val_max,
                "minimum_max": min_val_max,
                "average_max": avg_val_max,
                "valid_max": val_val_max
            }
            df = df.append(result_dict, ignore_index=True)

        df_max = df[["maximum_min", "maximum_avg", "maxmimum_max"]].copy()
        df_min = df[["minimum_min", "minimum_avg", "minimum_max"]].copy()
        df_avg = df[["average_min", "average_avg", "average_max"]].copy()
        df_valid = df[["valid_min", "valid_avg", "valid_max"]].copy()

        df_save_file_max = os.path.join("logs", folder, "finale_results_max.csv")
        df_max.to_csv(df_save_file_max, index=False, sep=';')
        plt_max = df_max.plot(title="Maximale Fitnesswerte pro Generation")
        plt_max.set_xlabel("Generationen")
        plt_max.set_ylabel("Fitness")
        fig_max = plt_max.get_figure()
        plt_file_path = os.path.join("logs", folder, "output_max.png")
        fig_max.savefig(plt_file_path)

        df_save_file_min = os.path.join("logs", folder, "finale_results_min.csv")
        df_min.to_csv(df_save_file_min, index=False, sep=';')
        plt_min = df_min.plot(title="Minimale Fitnesswerte pro Generation")
        plt_min.set_xlabel("Generationen")
        plt_min.set_ylabel("Fitness")
        fig_min = plt_min.get_figure()
        plt_file_path = os.path.join("logs", folder, "output_min.png")
        fig_min.savefig(plt_file_path)

        df_save_file_avg = os.path.join("logs", folder, "finale_results_avg.csv")
        df_avg.to_csv(df_save_file_avg, index=False, sep=';')
        plt_avg = df_avg.plot(title="Durchschnitt Fitnesswerte pro Generation")
        plt_avg.set_xlabel("Generationen")
        plt_avg.set_ylabel("Fitness")
        fig_avg = plt_avg.get_figure()
        plt_file_path = os.path.join("logs", folder, "output_avg.png")
        fig_avg.savefig(plt_file_path)

        df_save_file_valid = os.path.join("logs", folder, "finale_results_valid.csv")
        df_valid.to_csv(df_save_file_valid, index=False, sep=';')
        plt_valid = df_valid.plot(title="Valide Individuen pro Generation in %")
        plt_valid.set_xlabel("Generationen")
        plt_valid.set_ylabel("Fitness")
        fig_valid = plt_valid.get_figure()
        plt_file_path = os.path.join("logs", folder, "output_valid.png")
        fig_valid.savefig(plt_file_path)

        plt.close('all')

        print("print")


    end = time.time()
    print(end - start)


