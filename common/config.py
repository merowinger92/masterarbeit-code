# Genetic Programming Parameters

gp_run_parameters = {
    "crossover_probability": 0.8,
    "mutation_probability": 0.05,
    "generations": 50,
    "population_size": 300,
    "tournament_size": 2,
    "minimum_depth_tree": 10,
    "maximum_depth_tree": 15,
    "minimum_depth_mutation": 1,
    "maximum_depth_mutation": 7,
    "random_seed": 69
}

sc2_params = {
    "SC2_map": "MoveToBeacon",
    "SC2_screen_size": 84,
    "SC2_minimap_size": 64,
    "SC2_rgb_screen": None,
    "SC2_rgb_minimap": None,
    "SC2_action_space": None,
    "SC2_use_feature_units": False,
    "SC2_step_mul": 8,
    "SC2_game_steps_per_episode": None,
    "SC2_disable_fog": False,
    "SC2_visualize": False
}

file_paths = {
    'checkpoint_directory': ['checkpoint']
}