

class Bool(object):  # https://github.com/DEAP/deap/pull/106#issuecomment-158790599
    TRUE = True
    FALSE = False

    @staticmethod
    def return_terminals():
        list_of_terminals = []
        true_terminal = [Bool.TRUE, Bool, None]
        list_of_terminals.append(true_terminal)
        false_terminal = [Bool.FALSE, Bool, None]
        list_of_terminals.append(false_terminal)
        return list_of_terminals


class Point(object):
    pass


class PointMinimap(object):
    pass


class SC2Fct(object):
    pass


class PointScreen(object):
    pass


class SelectUnitID(object):

    @staticmethod
    def return_terminals():
        list_of_terminals = []
        for i in range(500):
            select = [i, SelectUnitID, None]
            list_of_terminals.append(select)
        return list_of_terminals


class ControlCroupID(object):

    @staticmethod
    def return_terminals():
        list_of_terminals = []
        for i in range(10):
            select = [i, ControlCroupID, None]
            list_of_terminals.append(select)
        return list_of_terminals


class UnloadID(object):

    @staticmethod
    def return_terminals():
        list_of_terminals = []
        for i in range(10):
            select = [i, UnloadID, None]
            list_of_terminals.append(select)
        return list_of_terminals


class BuildQueueID(object):

    @staticmethod
    def return_terminals():
        list_of_terminals = []
        for i in range(10):
            select = [i, BuildQueueID, None]
            list_of_terminals.append(select)
        return list_of_terminals


class SelectWorkerOpts(object):
    SELECT = "select"
    ADD = "add"
    SELECT_ALL = "select_all"
    ADD_ALL = "add_all"

    @staticmethod
    def return_terminals():
        list_of_terminals = []
        # prmitiveSetTyped.addTerminael(variable_function, return_Type, optional_Name) the name is mostly None
        select = [SelectWorkerOpts.SELECT, SelectWorkerOpts, None]
        list_of_terminals.append(select)
        add = [SelectWorkerOpts.ADD, SelectWorkerOpts, None]
        list_of_terminals.append(add)
        select_all = [SelectWorkerOpts.SELECT_ALL, SelectWorkerOpts, None]
        list_of_terminals.append(select_all)
        add_all = [SelectWorkerOpts.ADD_ALL, SelectWorkerOpts, None]
        list_of_terminals.append(add_all)
        return list_of_terminals


class SelectUnitAction(object):
    SELECT = "select"
    DESELECT = "deselect"
    SELECT_ALL_TYPE = "select_all_type"
    DESELECT_ALL_TYPE = "deselect_all_type"

    @staticmethod
    def return_terminals():
        list_of_terminals = []
        select = [SelectUnitAction.SELECT, SelectUnitAction, None]
        list_of_terminals.append(select)
        deselect = [SelectUnitAction.DESELECT, SelectUnitAction, None]
        list_of_terminals.append(deselect)
        select_all_type = [SelectUnitAction.SELECT_ALL_TYPE, SelectUnitAction, None]
        list_of_terminals.append(select_all_type)
        deselect_all_type = [SelectUnitAction.DESELECT_ALL_TYPE, SelectUnitAction, None]
        list_of_terminals.append(deselect_all_type)
        return list_of_terminals


class ControlGroupOpts(object):
    RECALL = "recall"
    SET = "set"
    APPEND = "append"
    SET_AND_STEAL = "set_and_steal"
    APPEND_AND_STEAL = "append_and_steal"

    @staticmethod
    def return_terminals():
        list_of_terminals = []
        recall = [ControlGroupOpts.RECALL, ControlGroupOpts, None]
        list_of_terminals.append(recall)
        set_ = [ControlGroupOpts.SET, ControlGroupOpts, None]
        list_of_terminals.append(set_)
        append = [ControlGroupOpts.APPEND, ControlGroupOpts, None]
        list_of_terminals.append(append)
        set_and_steal = [ControlGroupOpts.SET_AND_STEAL, ControlGroupOpts, None]
        list_of_terminals.append(set_and_steal)
        append_and_steal = [ControlGroupOpts.APPEND_AND_STEAL, ControlGroupOpts, None]
        list_of_terminals.append(append_and_steal)
        return list_of_terminals


class SelectPtOpts(object):
    SELECT = "select"
    TOGGLE = "toggle"
    SELECT_ALL_TYPE = "select_all_type"
    ADD_ALL_TYPE = "add_all_type"

    @staticmethod
    def return_terminals():
        list_of_terminals = []
        select = [SelectPtOpts.SELECT, SelectPtOpts, None]
        list_of_terminals.append(select)
        toggle = [SelectPtOpts.TOGGLE, SelectPtOpts, None]
        list_of_terminals.append(toggle)
        select_all_type = [SelectPtOpts.SELECT_ALL_TYPE, SelectPtOpts, None]
        list_of_terminals.append(select_all_type)
        add_all_type = [SelectPtOpts.ADD_ALL_TYPE, SelectPtOpts, None]
        list_of_terminals.append(add_all_type)
        return list_of_terminals


class SelectedValues(object):
    UNSELECTED = 0
    SELECTED = 1

    @staticmethod
    def return_terminals():
        list_of_terminals = []
        unselected = [SelectedValues.UNSELECTED, SelectedValues, None]
        list_of_terminals.append(unselected)
        selected = [SelectedValues.SELECTED, SelectedValues, None]
        list_of_terminals.append(selected)
        return list_of_terminals


class PlayerRelativeIDs(object):
    NONE = 0
    SELF = 1
    ALLY = 2
    NEUTRAL = 3
    ENEMY = 4

    @staticmethod
    def return_terminals():
        list_of_terminals = []
        none = [PlayerRelativeIDs.NONE, PlayerRelativeIDs, None]
        list_of_terminals.append(none)
        self = [PlayerRelativeIDs.SELF, PlayerRelativeIDs, None]
        list_of_terminals.append(self)
        ally = [PlayerRelativeIDs.ALLY, PlayerRelativeIDs, None]
        list_of_terminals.append(ally)
        neutral = [PlayerRelativeIDs.NEUTRAL, PlayerRelativeIDs, None]
        list_of_terminals.append(neutral)
        enemy = [PlayerRelativeIDs.ENEMY, PlayerRelativeIDs, None]
        list_of_terminals.append(enemy)
        return list_of_terminals


class VisibilityIDs(object):
    HIDDEN = 0
    SEEN = 1
    VISIBLE = 2

    @staticmethod
    def return_terminals():
        list_of_terminals = []
        hidden = [VisibilityIDs.HIDDEN, VisibilityIDs, None]
        list_of_terminals.append(hidden)
        seen = [VisibilityIDs.SEEN, VisibilityIDs, None]
        list_of_terminals.append(seen)
        visible = [VisibilityIDs.VISIBLE, VisibilityIDs, None]
        list_of_terminals.append(visible)
        return list_of_terminals


class EffectsID(object):
    PsiStorm = 1
    GuardianShield = 2
    TemporalFieldGrowing = 3
    TemporalField = 4
    ThermalLance = 5
    ScannerSweep = 6
    NukeDot = 7
    LiberatorDefenderZoneSetup = 8
    LiberatorDefenderZone = 9
    BlindingCloud = 10
    CorrosiveBile = 11
    LurkerSpines = 12

    @staticmethod
    def return_terminals():
        list_of_terminals = []
        # PsiStorm = 1
        # GuardianShield = 2
        # TemporalFieldGrowing = 3
        # TemporalField = 4
        # ThermalLance = 5
        # ScannerSweep = 6
        # NukeDot = 7
        # LiberatorDefenderZoneSetup = 8
        # LiberatorDefenderZone = 9
        # BlindingCloud = 10
        # CorrosiveBile = 11
        # LurkerSpines = 12
        psi_storm = [EffectsID.PsiStorm, EffectsID, None]
        list_of_terminals.append(psi_storm)
        guardian_shield = [EffectsID.GuardianShield, EffectsID, None]
        list_of_terminals.append(guardian_shield)
        temporal_field_growing = [EffectsID.TemporalFieldGrowing, EffectsID, None]
        list_of_terminals.append(temporal_field_growing)
        temporal_field = [EffectsID.TemporalField, EffectsID, None]
        list_of_terminals.append(temporal_field)
        thermal_lance = [EffectsID.ThermalLance, EffectsID, None]
        list_of_terminals.append(thermal_lance)
        scanner_sweep = [EffectsID.ScannerSweep, EffectsID, None]
        list_of_terminals.append(scanner_sweep)
        nuke_dot = [EffectsID.NukeDot, EffectsID, None]
        list_of_terminals.append(nuke_dot)
        liberator_defender_zone_setup = [EffectsID.LiberatorDefenderZoneSetup, EffectsID, None]
        list_of_terminals.append(liberator_defender_zone_setup)
        liberator_defender_zone = [EffectsID.LiberatorDefenderZone, EffectsID, None]
        list_of_terminals.append(liberator_defender_zone)
        blinding_cloud = [EffectsID.BlindingCloud, EffectsID, None]
        list_of_terminals.append(blinding_cloud)
        corrosive_bile = [EffectsID.CorrosiveBile, EffectsID, None]
        list_of_terminals.append(corrosive_bile)
        lukker_spins = [EffectsID.LurkerSpines, EffectsID, None]
        list_of_terminals.append(lukker_spins)
        return list_of_terminals
