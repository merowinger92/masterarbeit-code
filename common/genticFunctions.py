import numpy as np


def if_then_else(condition, out1, out2):
    if callable(condition) and callable(out1) and callable(out2):
        return out1() if condition() else out2()
    elif callable(condition) and callable(out1):
        return out1() if condition() else out2
    elif callable(condition) and callable(out2):
        return out1 if condition() else out2()
    elif callable(out1) and callable(out2):
        return out1() if condition else out2()
    elif callable(condition):
        return out1 if condition() else out2
    elif callable(out1):
        return out1() if condition else out2
    elif callable(out2):
        return out1 if condition else out2()

    return out1 if condition else out2


def genetic_and(condition1, condition2):
    if callable(condition1) and callable(condition2):
        return condition1() and condition2()
    if callable(condition1):
        return condition1() and condition2
    if callable(condition2):
        return condition1 and condition2()

    return condition1 and condition2


def helper_ndarray():
    return np.zeros(2)


def generic_helper_primitive(generic_var):
    if callable(generic_var):
        return generic_var()
    return generic_var
