import pickle
import glob
import os
import datetime
from scipy.spatial import distance

MAX_DISTANCE_SCREEN = []
MAX_DISTANCE_MINIMAP = []


class Globals(object):
    pass


def load_checkpoint(checkpoint_dir_regex):
    """
    method checks if a checkpoint exists in *checkpoint_dir_regex* and returns the latest one, otherwise false

    :param checkpoint_dir_regex:
    :return: dictionary with checkpoint information if a checkpoint exists, otherwise false
    """
    list_of_files = glob.glob(checkpoint_dir_regex)
    if len(list_of_files) > 0:
        latest_chkpt_file = max(list_of_files, key=os.path.getctime)
        with open(latest_chkpt_file, "r") as chkpt_file:
            chkpt = pickle.load(chkpt_file)
        return chkpt
    return False


def save_checkpoint_to_file(checkpoint_dict, checkpoint_dir, agent_name):
    current_time_str = datetime.datetime.now().strftime("%d%m%y%H%M%S")
    file_name = agent_name + current_time_str + ".pkl"
    if not os.path.exists(checkpoint_dir):
        os.makedirs(checkpoint_dir)
    full_path = os.path.join(checkpoint_dir, file_name)
    with open(full_path, "wb") as chkpt_file:
        pickle.dump(checkpoint_dict, chkpt_file)


def delete_older_states(checkpoint_dir_regex, agent_name):
    files_to_match = checkpoint_dir_regex + agent_name + '*'
    list_of_files = glob.glob(files_to_match)
    if len(list_of_files) > 0:
        for file in list_of_files:
            os.remove(file)


if __name__ == "__main__":

    print("sad")
