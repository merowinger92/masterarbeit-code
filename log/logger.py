import glob
import os
from common import config
import pickle

LOG_DIRECTORY = "logs"
DEFAULT_LOG_FOLDER = "default"

FINISHED_RUNS = 0


def set_and_create_log_folder(folder_name):
    global DEFAULT_LOG_FOLDER
    DEFAULT_LOG_FOLDER = folder_name
    reg_ex = LOG_DIRECTORY + "/" + folder_name + "*"
    files = glob.glob(reg_ex)
    global FINISHED_RUNS
    FINISHED_RUNS = len(files)
    new_dir = DEFAULT_LOG_FOLDER + "-" + str(FINISHED_RUNS)
    os.makedirs(os.path.join(LOG_DIRECTORY, new_dir))


def set_log_folder_for_contiuation_run(folder_name):
    global DEFAULT_LOG_FOLDER
    DEFAULT_LOG_FOLDER = folder_name
    reg_ex = LOG_DIRECTORY + "/" + folder_name + "*"
    files = glob.glob(reg_ex)
    global FINISHED_RUNS
    FINISHED_RUNS = len(files) - 1


def get_run_root_directory_str():
    return LOG_DIRECTORY + "/" + DEFAULT_LOG_FOLDER + "-" + str(FINISHED_RUNS)


def get_run_root_directory_path():
    log_fldr = DEFAULT_LOG_FOLDER + "-" + str(FINISHED_RUNS)
    return [LOG_DIRECTORY, log_fldr]


def log_individuals(generation, individuals, postfix=None):
    for idx, ind in enumerate(individuals):
        log_individual(idx, generation, ind, postfix)


def log_individual(entry_id, generation, individual, postfix=None):
    filename_prefix = "run-"
    if postfix:
        filename = filename_prefix + str(generation) + "-" + str(postfix) + ".csv"
    else:
        filename = filename_prefix + str(generation) + ".csv"
    # run results without individual
    root_dir_str = get_run_root_directory_str()
    full_file_name = root_dir_str + "/" + filename
    if not os.path.isfile(full_file_name):
        # add header
        with open(full_file_name, 'a+') as file:
            file.write('ID; Individuals; Fitness\n')

    with open(full_file_name, 'a+') as csv_file:
        csv_file.write(str(entry_id) + ';' + str(individual) + ';' + str(individual.fitness.values[0]) + '\n')


def log_run(run_stats, postfix=None):
    filename_prefix = "summary"
    if postfix:
        filename = filename_prefix + "-" + str(postfix) + ".txt"
    else:
        filename = filename_prefix + ".txt"
    full_file_name = LOG_DIRECTORY + "/" + DEFAULT_LOG_FOLDER + "-" + str(FINISHED_RUNS) + "/" + filename
    with open(full_file_name, 'a+') as csv_file:
        csv_file.write(run_stats)


def log_selection_results(generation, population, postfix=None):
    for idx, ind in enumerate(population):
        log_single_selection(idx, generation, ind, postfix)


def log_single_selection(entry_id, generation, individual, postfix=None):
    filename_prefix = "selection-"
    if postfix:
        filename = filename_prefix + str(generation) + "-" + str(postfix) + ".csv"
    else:
        filename = filename_prefix + str(generation) + ".csv"
    full_file_name = LOG_DIRECTORY + "/" + DEFAULT_LOG_FOLDER + "-" + str(FINISHED_RUNS) + "/" + filename
    if not os.path.isfile(full_file_name):
        # add header
        with open(full_file_name, 'a+') as file:
            file.write('ID; Individual; Fitness\n')

    with open(full_file_name, 'a+') as csv_file:
        csv_file.write(str(entry_id) + ';' + str(individual) + ';' + str(individual.fitness.values[0]) + '\n')


def log_gp_parameters():
    filename_prefix = "GP_Parameter"
    filename = filename_prefix + ".csv"
    full_file_name = LOG_DIRECTORY + "/" + DEFAULT_LOG_FOLDER + "-" + str(FINISHED_RUNS) + "/" + filename
    if not os.path.isfile(full_file_name):
        # add header
        with open(full_file_name, 'a+') as file:
            file.write('Parameter Name; Value\n')
    with open(full_file_name, 'a+') as csv_file:
        for key, value in config.gp_run_parameters.items():
            csv_file.write(key + ';' + str(value) + '\n')


def log_best_individual(best_ind, postfix=None):
    if postfix:
        filename = "best_individual-" + str(postfix) + ".txt"
    else:
        filename = "best_individual.txt"
    full_file_name = LOG_DIRECTORY + "/" + DEFAULT_LOG_FOLDER + "-" + str(FINISHED_RUNS) + "/" + filename
    with open(full_file_name, 'a+') as txt_file:
        txt_file.write(str(best_ind))


def log_exception_and_success(result, individual, postfix=None):
    if postfix:
        filename = "except_success_ind-" + str(postfix) + ".csv"
    else:
        filename = "except_success_ind.csv"
    full_file_name = LOG_DIRECTORY + "/" + DEFAULT_LOG_FOLDER + "-" + str(FINISHED_RUNS) + "/" + filename
    with open(full_file_name, 'a+') as csv_file:
        line = str(result) + ';' + str(individual) + '\n'
        csv_file.write(line)


def log_function_discrepancies(seen_func_set, user_funcs, postfix=None):
    if postfix:
        filename = "discrepancy-" + postfix + ".log"
    else:
        filename = "discrepancy-" + postfix + ".log"

    full_file_name = LOG_DIRECTORY + "/" + DEFAULT_LOG_FOLDER + "-" + str(FINISHED_RUNS) + "/" + filename
    with open(full_file_name, 'a+') as log_file:
        line = 'Seen Funcs: \n'
        for _ in seen_func_set:
            line += str(_) + '\n'

        line += 'User Funcs \n'
        for _ in user_funcs:
            line += str(_) + '\n'
        log_file.write(line)


if __name__ == "__main__":
    print("ff")

