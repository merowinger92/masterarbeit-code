pattern = '        single_primitive = [self.{function_name}, {function_list}, Function, None]\n'
pattern += '        list_of_primitives.append(single_primitive)\n'


def extract_function_and_args(line_of_files):
    idx_func_start = line_of_files.find('"') + 1
    idx_func_last = line_of_files.rfind('"')
    function_name = line_of_files[idx_func_start:idx_func_last]
    args_name = line_of_files.split(',')[2]
    if args_name.endswith(')'):
        args_name = args_name[:-1]
    return function_name, args_name

def calc_function_list(func_type):
    return_func = "[]"
    if func_type == " no_op":
        pass
    elif func_type == " move_camera":
        return_func = "[genetitcTypes.PointMinimap]"
    elif func_type == " select_point":
        return_func = "[genetitcTypes.SelectPtOpts, genetitcTypes.PointScreen]"
    elif func_type == " select_rect":
        return_func = "[genetitcTypes.Bool, genetitcTypes.PointScreen, genetitcTypes.PointScreen]"
    elif func_type == " select_unit":
        return_func = "[genetitcTypes.SelectUnitAction, genetitcTypes.SelectUnitID]"
    elif func_type == " control_group":
        return_func = "[genetitcTypes.ControlGroupOpts, genetitcTypes.ControlCroupID]"
    elif func_type == " select_idle_worker":
        return_func = "[genetitcTypes.SelectWorkerOpts]"
    elif func_type == " select_army":
        return_func = "[genetitcTypes.Bool]"
    elif func_type == " select_warp_gates":
        return_func = "[genetitcTypes.Bool]"
    elif func_type == " unload":
        return_func = "[genetitcTypes.UnloadID]"
    elif func_type == " build_queue":
        return_func = "[genetitcTypes.BuildQueueID]"
    elif func_type == " cmd_quick":
        return_func = "[genetitcTypes.Bool]"
    elif func_type == " cmd_screen":
        return_func = "[genetitcTypes.Bool, genetitcTypes.PointScreen]"
    elif func_type == " cmd_minimap":
        return_func = "[genetitcTypes.Bool, genetitcTypes.PointMinimap]"
    elif func_type == " autocast" or func_type == " select_larva" or func_type == " no_op":
        pass
    else:
        raise Exception
    return return_func


def build_primitive(pattern_struct, func_name, arg_name):
    pattern_struct = pattern_struct.replace("{function_name}", func_name.lower())
    list_of_args = calc_function_list(arg_name)
    pattern_struct = pattern_struct.replace("{function_list}", list_of_args)
    return pattern_struct


if __name__ == "__main__":
    lines = []
    with open('Primitives.txt', 'r') as f:
        for line in f:
            act, arg = extract_function_and_args(line)
            new_line = build_primitive(pattern, act, arg)
            lines.append(new_line)

    with open('PrimitivesGenerated.txt', 'w') as outfile:
        for line in lines:
            outfile.write(line)

    print("ready")
