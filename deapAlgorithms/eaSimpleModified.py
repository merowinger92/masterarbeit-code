import random

from deap import tools
from deap import algorithms

from common import utils
from common import config
from common.process_utilities import PoisonPill

import os
import time

from log import logger
import multiprocessing as mp
from multiprocessing import Queue, JoinableQueue, Pool
import sys


def eaSimple(population, toolbox, cxpb, mutpb, ngen, stats=None,
             halloffame=None, postfix=None, map="DEFAULT", make_chekpoint_every_generations=5, verbose=__debug__):
    """This algorithm reproduce the simplest evolutionary algorithm as
    presented in chapter 7 of [Back2000]_.

    :param population: A list of individuals.
    :param toolbox: A :class:`~deap.base.Toolbox` that contains the evolution
                    operators.
    :param cxpb: The probability of mating two individuals.
    :param mutpb: The probability of mutating an individual.
    :param ngen: The number of generation.
    :param stats: A :class:`~deap.tools.Statistics` object that is updated
                  inplace, optional.
    :param halloffame: A :class:`~deap.tools.HallOfFame` object that will
                       contain the best individuals, optional.
    :param verbose: Whether or not to log the statistics.
    :returns: The final population
    :returns: A class:`~deap.tools.Logbook` with the statistics of the
              evolution

    The algorithm takes in a population and evolves it in place using the
    :meth:`varAnd` method. It returns the optimized population and a
    :class:`~deap.tools.Logbook` with the statistics of the evolution. The
    logbook will contain the generation number, the number of evalutions for
    each generation and the statistics if a :class:`~deap.tools.Statistics` is
    given as argument. The *cxpb* and *mutpb* arguments are passed to the
    :func:`varAnd` function. The pseudocode goes as follow ::

        evaluate(population)
        for g in range(ngen):
            population = select(population, len(population))
            offspring = varAnd(population, toolbox, cxpb, mutpb)
            evaluate(offspring)
            population = offspring

    As stated in the pseudocode above, the algorithm goes as follow. First, it
    evaluates the individuals with an invalid fitness. Second, it enters the
    generational loop where the selection procedure is applied to entirely
    replace the parental population. The 1:1 replacement ratio of this
    algorithm **requires** the selection procedure to be stochastic and to
    select multiple times the same individual, for example,
    :func:`~deap.tools.selTournament` and :func:`~deap.tools.selRoulette`.
    Third, it applies the :func:`varAnd` function to produce the next
    generation population. Fourth, it evaluates the new individuals and
    compute the statistics on this population. Finally, when *ngen*
    generations are done, the algorithm returns a tuple with the final
    population and a :class:`~deap.tools.Logbook` of the evolution.

    .. note::

        Using a non-stochastic selection method will result in no selection as
        the operator selects *n* individuals from a pool of *n*.

    This function expects the :meth:`toolbox.mate`, :meth:`toolbox.mutate`,
    :meth:`toolbox.select` and :meth:`toolbox.evaluate` aliases to be
    registered in the toolbox.

    .. [Back2000] Back, Fogel and Michalewicz, "Evolutionary Computation 1 :
       Basic Algorithms and Operators", 2000.
    """
    checkpoint_directory = os.path.join(*config.file_paths['checkpoint_directory'])

    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

    start_generation = 1

    # Evaluate the individuals with an invalid fitness distributed over threads
    invalid_ind = [(idx, ind) for idx, ind in enumerate(population) if not ind.fitness.valid]
    # setup queue und multi pool
    inv_ind_quue = JoinableQueue(len(invalid_ind) + 1)
    man = mp.Manager()
    out_q = man.list()
    process_pool = Pool(os.cpu_count(), toolbox.evaluate, (inv_ind_quue, out_q))

    # put all Individuals to a queue which is later evaluated over different processes
    for ind in invalid_ind:
        inv_ind_quue.put(ind)

    # wait until queue is processed
    inv_ind_quue.join()
    assert len(invalid_ind) == len(out_q)

    # extend the already evaluated inds by the inds from the list
    while len(out_q) > 0:
        entry = out_q.pop()
        population[entry[0]] = entry[1]

    # out_q has to be empty
    assert len(out_q) == 0

    # log population
    logger.log_individuals(0, population, postfix)

    if halloffame is not None:
        halloffame.update(population)

    record = stats.compile(population) if stats else {}
    logbook.record(gen=0, nevals=len(invalid_ind), **record)
    if verbose:
        print(logbook.stream)

    # Begin the generational process
    for gen in range(start_generation, ngen + 1):

        # Select the next generation individuals
        offspring = toolbox.select(population, len(population))

        logger.log_selection_results(gen, offspring, postfix)

        # Vary the pool of individuals
        offspring = algorithms.varAnd(offspring, toolbox, cxpb, mutpb)

        # Evaluate the individuals with an invalid fitness distributed over threads
        invalid_ind = [(idx, ind) for idx, ind in enumerate(offspring) if not ind.fitness.valid]

        # inv_ind_quue = JoinableQueue(len(invalid_ind) + 1)
        for ind in invalid_ind:
            inv_ind_quue.put(ind)

        # wait until all items are processed
        inv_ind_quue.join()
        assert len(invalid_ind) == len(out_q)

        # extend the already evaluated inds by the inds from the list
        while len(out_q) > 0:
            entry = out_q.pop()
            offspring[entry[0]] = entry[1]

        # out_q has to be empty
        assert len(out_q) == 0

        # Update the hall of fame with the generated individuals
        if halloffame is not None:
            halloffame.update(offspring)

        # Replace the current population by the offspring
        population[:] = offspring

        # log population
        logger.log_individuals(gen, population, postfix)

        # Append the current generation statistics to the logbook
        record = stats.compile(population) if stats else {}
        logbook.record(gen=gen, nevals=len(invalid_ind), **record)
        if verbose:
            print(logbook.stream)

        #  different to original: safe checkpoint
        chkpt = dict(population=population, generation=gen, halloffame=halloffame,
                     logbook=logbook, rnd_state=random.getstate())

    # log a complete run
    logger.log_run(logbook.__str__(), postfix)

    # add Poison Pill to Queue to kill left over processes an all SC2 subprocesses
    for i in range(os.cpu_count()):
        inv_ind_quue.put(PoisonPill())

    inv_ind_quue.join()
    process_pool.terminate()
    process_pool.join()

    return population, logbook
